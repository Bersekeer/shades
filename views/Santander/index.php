<!DOCTYPE html>
<html lang="en">
<head>
	
	<?php include "views/partial_views/_styles.php"; ?>
	<title>Santander-response Adhara Cancún</title>
	<!-- Estilos vista piscina -->
	<link rel="stylesheet" href="/css/404.min.css">

</head>
<body style="background-image: url('/img/background.png');">

	<?php include "lang/languaje.php"; ?>	

	<!-- Navbar mobile -->
    <?php include "views/partial_views/_navbar_mobile.php"; ?>

	<!-- Redes Sociales -->
	<?php include "views/partial_views/_redes.php"; ?>
	<style>
		h1{
			font-weight: 400;
		}
		.secure-items{
			display: inline-flex;
			list-style: none;
		}

		.secure-items li{
			padding: 5px;
			margin-right: 5px;
			margin-left: 5px;
		}
		#first-secure{
			float: right;
		}

		#secure-payment{
			width:100px;
		}
		#secure-card{
			width: 200px;
		}
		#secure-ssl{
			width: 80px;
		}
		#secure-comodo{
			width: 100px;
			margin-top: 15px;
		}
	</style>
	<div id="general">
		<!-- Navbar -->
		<?php include "views/partial_views/_navbar.php"; ?>
		
		<div class="container">
			
			<div  id="wrapper-content" style="padding-top: 60px;">
				<div class="row" style="margin:0px;">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<ul style="display: inline-flex;list-style: none;" class="secure-items" id="first-secure">
							<li><img src="/img/new_items/pago_seguro.png" id="secure-payment" alt="Pago Seguro"></li>
							<li><img src="/img/new_items/pago_card.png" id="secure-card" alt="Pago Tarjeta"></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<ul style="display: inline-flex;list-style: none;" class="secure-items" id="last-secure">
							<li><img src="/img/new_items/ssl.png" id="secure-ssl" alt="SSL"></li>
							<li><img src="/img/new_items/comodo.png" id="secure-comodo" alt="Comodo SSL"></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-12">
						<label for="" style="color: #473934;">Nota: Apartir de este punto, no refresque el navegador</label>
					</div>
				</div>
				<div class="method_payment" style="width: 550px;display: block;margin:0px auto;margin-top: 80px;margin-bottom: 50px;">
					

					<?php 
						echo '<iframe class="embed-frame" src="'.base64_decode($_GET['nb_url']).'" allowfullscreen></iframe>';
						//unset($_COOKIE['nb_url']);
					?>
				</div>

				
				
				<div id="wrapper_footer">
					<?php include "views/partial_views/_footer.php"; ?>
				</div>
			</div>
		</div>
	</div>
	 <!-- Site Overlay 
    <div class="site-overlay"></div>-->
	<!-- Preloading -->
	<!-- <?php include "views/partial_views/_preloading.php"; ?> -->

</body>

<?php include "views/partial_views/_scripts.php"; ?>


</html>