<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="header_adhara" style="height: 60px;">
  <div id="head_">
    
    <a class="navbar-brand" href="/"><img src="/img/logos/adhara_logo.png" alt="Hotel Adhara Hacienda Cancun" id="logo"></a>
 

    <div class="navbar" id="mob_menu">
      <ul class="navbar-nav ml-auto" id="menu_desktop">
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-home" href="/"><span><?php echo $_GLOBALS['header_home']; ?></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-hotel" href="/somos"><span><?php echo $_GLOBALS['header_somos']; ?></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-grupos" href="/grupos"><span><?php echo $_GLOBALS['header_grupos']; ?></span></a>
        </li>
        <li class="nav-item">

          <div class="dropdown">
            <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-shuttle" href="#" role="button" id="services_drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <span><?php echo $_GLOBALS['header_servicios']; ?></span>
            </a>

            <div class="dropdown-menu" aria-labelledby="services_drop" id="services_menu">
              <a class="dropdown-item" href="/shuttle"><?php echo $_GLOBALS['header_playa']; ?></a>
              <a class="dropdown-item" href="/pool"><?php echo $_GLOBALS['header_alberca']; ?></a>
              <a class="dropdown-item" href="/adharagrill"><?php echo $_GLOBALS['header_restaurante']; ?></a>
              <a class="dropdown-item" href="cocodrillos"><?php echo $_GLOBALS['header_cocodrillo']; ?></a>
              <a class="dropdown-item" href="/bussiness_center"><?php echo $_GLOBALS['header_bussiness']; ?></a>
              <a class="dropdown-item" href="/lobby"><?php echo $_GLOBALS['header_lobby']; ?></a>
              <a class="dropdown-item" href="/gym"><?php echo $_GLOBALS['header_gym']; ?></a>
              <a class="dropdown-item" href="/showroom"><?php echo $_GLOBALS['header_rooms']; ?></a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-eventos" href="/eventos"><span><?php echo $_GLOBALS['header_eventos']; ?></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5bb icon-estrella" href="/clubestrella"><span>CLUB ESTRELLA</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-contacto" href="/contacto"><span><?php echo $_GLOBALS['header_meet_us']; ?></span></a>
        </li>
        <!-- <li class="nav-item">
          <div class="dropdown">
            <a class="nav-link d-flex align-items-center butt btn-5 btn-5b icon-contacto" href="#" role="button" id="contact_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <span>CONOCENOS</span>
            </a>
        
            <div class="dropdown-menu" aria-labelledby="contact_menu">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div> -->
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center" href="https://api.whatsapp.com/send?phone=529981221861" target="_blank"><img src="/img/logos/whatsapp.png" alt="Whatsapp" style="width: 35px;"></a>
        </li>
        <li class="nav-item d-flex align-items-center">
          <a class="nav-link" href="#" id="lang">
            <img src="<?php echo $_GLOBALS['flag_src']; ?>" alt="<?php echo $_GLOBALS['flag_alt']; ?>" style="width: 32px;padding-top: 5px;">
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto" id="menu_mob" style="flex-direction:row;">
        <li class="nav-item">
          <a class="nav-link d-flex align-items-center" href="https://api.whatsapp.com/send?phone=529981221861" target="_blank"><img src="/img/logos/whatsapp.png" alt="Whatsapp" style="width: 25px;"></a>
        </li>
        <li class="nav-item d-flex align-items-center">
          <a class="nav-link" href="#" id="lang_mob">
            <img src="<?php echo $_GLOBALS['flag_src']; ?>" alt="<?php echo $_GLOBALS['flag_alt']; ?>" style="width: 23px;padding-top: 7px;">
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-item d-flex align-items-center menu-btn-1 h--left">
            <i class="fas fa-bars"></i>
          </a>
        </li>
      </ul>
      
    </div>
  </div>
</nav>