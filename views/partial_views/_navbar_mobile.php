<nav id="menu-1" class="offside">
    <a href="#" class="icon icon--cross menu-btn-1--close h--right">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <ul id="side_menu">
        <li><a href="/"><i class="fas fa-home"></i> <?php echo $_GLOBALS['header_home']; ?></a></li>
        <li><a href="/somos"><i class="fas fa-hotel"></i> <?php echo $_GLOBALS['header_somos']; ?></a></li>
        <li><a href="/grupos"><i class="fas fa-users"></i> <?php echo $_GLOBALS['header_grupos']; ?></a></li>
        <li><a href="/pool"><i class="fas fa-swimming-pool"></i></i><?php echo $_GLOBALS['header_alberca']; ?></a></li>
        <li><a href="/shuttle"><i class="fas fa-shuttle-van"></i></i><?php echo $_GLOBALS['header_playa']; ?></a></li>
        <li><a href="/adharagrill"><i class="fas fa-utensils"></i></i><?php echo $_GLOBALS['header_restaurante']; ?></a></li>
        <li><a href="cocodrillos"><i class="fas fa-utensils"></i></i><?php echo $_GLOBALS['header_cocodrillo']; ?></a></li>
        <li><a href="/bussiness_center"><i class="fas fa-business-time"></i></i><?php echo $_GLOBALS['header_bussiness']; ?></a></li>
        <li><a href="/lobby"><i class="fas fa-glass-martini-alt"></i></i><?php echo $_GLOBALS['header_lobby']; ?></a></li>
        <li><a href="/gym"><i class="fas fa-dumbbell"></i></i><?php echo $_GLOBALS['header_gym']; ?></a></li>
        <li><a href="/showroom"><i class="fas fa-bed"></i></i><?php echo $_GLOBALS['header_rooms']; ?></a></li>
        <li><a href="/eventos"><i class="fas fa-glass-cheers"></i><?php echo $_GLOBALS['header_eventos']; ?></a></li>
        <li><a href="/clubestrella"><i class="fas fa-haykal"></i>CLUB ESTRELLA</a></li>
        <li><a href="/contacto"><i class="fas fa-map-marked-alt"></i> <?php echo $_GLOBALS['header_contacto']; ?></a></li>
        
    </ul>
</nav>