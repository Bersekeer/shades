
<?php 
$_GLOBALS['lng'] = 0;
/* Sección del header */
$_GLOBALS['header_home'] = "INICIO";
$_GLOBALS['header_somos'] = "SOMOS";
$_GLOBALS['header_rooms'] = "HABITACIONES";
$_GLOBALS['header_servicios'] = "SERVICIOS";
$_GLOBALS['header_alberca'] = "ALBERCA";
$_GLOBALS['header_playa'] = "TRANSPORTACIÓN";
$_GLOBALS['header_restaurante'] = "ADHARA GRILL";
$_GLOBALS['header_cocodrillo'] = "COCODRILLOS";
$_GLOBALS['header_bussiness'] = "CENTRO DE NEGOCIOS";
$_GLOBALS['header_lobby'] = "LOBBY BAR";
$_GLOBALS['header_gym'] = "GIMNASIO";
$_GLOBALS['header_eventos'] = "EVENTOS";
$_GLOBALS['header_contacto'] = "CONTACTO";
$_GLOBALS['header_grupos'] = "GRUPOS";
$_GLOBALS['header_meet_us'] = "CONÓCENOS";
$_GLOBALS['header_whats'] = "/img/whatsapp-es.png";
$_GLOBALS['keywords']= "Reserva en línea tu hospedaje con nuestra tarifa mágica, somos tu mejor  opción en el centro de Cancún, nuestra ubicación es privilegiada, viajeros de negocios y placer, habitaciones muy amplias y confortables, Wifi gratis, alberca, gimnasio, desayuno buffet, transportación gratis en horario establecido sujeto a  disponibilidad, contamos  con nuestro  programa de  lealtad  Club  Estrella ,  acumula  puntos  y  obtén recompensas, reserva  en  nuestro  sitio  oficial y  obtén  grandes beneficios, hoteles en  Cancun, hoteles en  el  centro de  Cancun, Hot sale  2019 ,buen  fin 2019";

$_GLOBALS['flag_src'] = "/img/flags/usa.png";
$_GLOBALS['flag_alt'] = "en";

/* Sección del Buscador */
$_GLOBALS['search-from'] = "Llegada";
$_GLOBALS['search-to'] = "Salida";
$_GLOBALS['search-rooms'] = "Cuartos";
$_GLOBALS['search-code'] = "Tarifa Especial";

$_GLOBALS['search-dates'] = "Fechas a Reservar";
$_GLOBALS['search-room'] = "Habitación";
$_GLOBALS['search-enter'] = "Buscar";
$_GLOBALS['search-apply'] = "Aplicar";
$_GLOBALS['search-adults'] = "Adultos";
$_GLOBALS['search-kids'] = "Menores";
$_GLOBALS['search-delete'] = "quitar";


/* Seccion de Tarifa del Dia */
$_GLOBALS['rates-taxes'] = "Impuestos incluidos";
$_GLOBALS['rates-whats'] = "Cotiza,Reserva y Paga por WhatsApp";

/* Imagen Home */
$_GLOBALS['url-home-800'] = "/img/places/home_mobile.png";
$_GLOBALS['url-home-500'] = "/img/places/home_mob.png";

/* Quick Search */

$_GLOBALS['quick-date'] = "Tarifa de Hoy<br>Incluye desayuno*";
$_GLOBALS['quick-date-mob'] = "Tarifa de Hoy";
$_GLOBALS['quick-price'] = "$ 1,320";
$_GLOBALS['quick-tarifa'] = "RESERVA CON</span> <br>TARIFA MÁGICA";
$_GLOBALS['quick-whats'] = "Cotiza, Reserva y Paga<br> por Whatsapp";
$_GLOBALS['quick-style'] = "margin-left:0px;";
$_GLOBALS['quick-font'] = "font-size:20px;";
$_GLOBALS['quick-offer'] = "Desayuno gratis hasta para 2 personas";

/* Página Home */
$_GLOBALS['home-tittle-1'] = "Bienvenido a";
$_GLOBALS['home-tittle-2'] = "Hotel Adhara Cancún";
$_GLOBALS['home-descrip'] = "Se encuentra en la ciudad de Cancún y su estupenda ubicación es muy conveniente para turistas y viajeros de negocios. Nos destacamos por la calidez de nuestra atención personalizada ofreciendo siempre un trato altamente profesional y eficiente.";
$_GLOBALS['home-descrip2'] = "La arquitectura y diseño de interiores colonial, es un ambiente ideal para pasar unos días de estancia muy cómodos, relajados y placenteros, contando con los servicios más modernos de la hotelería.";

$_GLOBALS['home-rooms'] = "NUESTRAS HABITACIONES";
$_GLOBALS['home-details'] = "Detalles";

$_GLOBALS['home-room-estandar'] = "Habitación Estándar";
$_GLOBALS['home-room-junior'] = "Habitación Superior";
$_GLOBALS['home-room-ejecutivo'] = "Habitación Ejecutívo";
$_GLOBALS['home-room-descrip'] = "Equipadas con modernos servicios para estancias de trabajo o descanso.";
$_GLOBALS['home-room-features'] = "La habitación cuenta con caja de seguridad y un cómodo escritorio de trabajo.";

$_GLOBALS['home-galeria-playa'] = "Transportación a la playa";
$_GLOBALS['home-galeria-interior'] = "Interiores & Arquitectura";
$_GLOBALS['home-galeria-lobby'] = "Lobby & Bar";
$_GLOBALS['home-galeria-restaurante'] = "Restaurante Adhara Grill";
$_GLOBALS['home-galeria-negocios'] = "Centro de Negocios";
$_GLOBALS['home-galeria-juntas'] = "Salónes de juntas";
$_GLOBALS['home-galeria-alberca'] = "Alberca";
$_GLOBALS['home-galeria-eventos'] = "Eventos & Convenciones";

$_GLOBALS['home-magia-h1'] = "¡Hacemos magia en cada detalle!";
$_GLOBALS['home-magia-p'] = "En Hotel Adhara nuestra filosofía de servicio se basa en la calidez humana y el trato personalizado hacia nuestros huéspedes.grandes anfitriones a nivel internacional.Corporativamente hemos decidido “hacer magia en cada detalle”, siendo éste nuestro lema y objetivo, trabajando en ofrecer confort y la satisfacción memorable a nuestros apreciados visitantes.";
$_GLOBALS['home-magia-h2'] = "¡Te garantizamos la mejor tarifa!";
$_GLOBALS['home-magia-p2'] = "Estamos completamente seguros de ofrecer esta promesa, porque desde nuestra página web estas haciendo un trato directo con nosotros y ningún intermediario o agencía de ventas externa, puede extender una mejor oferta que la que nosotros te presentamos desde nuestro eficiente y seguro sistema de reservaciones online.";

$_GLOBALS['home-magia-h3'] = "TRANSPORTACIÓN";
$_GLOBALS['home-magia-p6'] = "Siendo huesped de nuestro hotel, usted cuenta con nuestro servicio de transportación hacia el aeropuerto y playas de Cancún.";
$_GLOBALS['home-magia-p7'] = "Reserva con nosotros y agenda tu transporte con anticipación*";
$_GLOBALS['home-magia-p3'] = "Contamos con un servicio diario en diferentes horarios muy convenientes según la temporada. Reserva con nosotros y agenda tu transporte con aticipación.";
$_GLOBALS['home-magia-p4'] = "Este servicio está sujeto a disponibilidad y horario.";
$_GLOBALS['home-magia-p5'] = "Aplican restricciones*";
$_GLOBALS['home-testimonio-h1'] = "Testimoniales";
$_GLOBALS['home-return'] = "Volver al inicio";

/* Página disponibilidad */
$_GLOBALS['room-taxes'] = "Todos los precios incluyen impuestos";
$_GLOBALS['room-label'] = "Habitaciones";
$_GLOBALS['room-adults'] = "Adultos";
$_GLOBALS['room-kids'] = " Niños";
$_GLOBALS['room-nights'] = " / Noches: ";
$_GLOBALS['room-text'] = "Llegando el ";
$_GLOBALS['room-text-2'] = " de ";
$_GLOBALS['room-text-3'] = " del ";
$_GLOBALS['room-text-4'] = " Saliendo el ";

$_GLOBALS['room-feature'] = "Wifi";
$_GLOBALS['room-feature-2'] = "Cafetera";
$_GLOBALS['room-feature-3'] = "Servicio Habitacion";
$_GLOBALS['room-feature-4'] = "Caja de Seguridad";
$_GLOBALS['room-feature-5'] = "Escritorio Trabajo";
$_GLOBALS['room-feature-plus'] = "Mas información";


$_GLOBALS['disponibilidad-room-estandar'] = "Habitación Estándar";
$_GLOBALS['disponibilidad-room-estandar-text'] = "Estas cómodas habitaciones cuentan con un práctico equipamiento que incluye una cama confortable para dormir plenamente, así como televisión de pantalla plana, cable, escritorio, silla ergonómica, así como acceso a Internet WiFi.";
$_GLOBALS['disponibilidad-room-superior'] = "Habitación Superior";
$_GLOBALS['disponibilidad-room-superior-text'] = "Nuestra Habitación Superior son reconocidas como las más amplias y confortables de su categoría en la zona centro de la ciudad. En Hotel Adhara Cancún brindamos un equipamiento moderno, con una gran variedad de servicios para sus estancias de trabajo o descanso.";
$_GLOBALS['disponibilidad-room-ejecutivo'] = "Habitación Ejecutivo";
$_GLOBALS['disponibilidad-room-ejecutivo-text'] = "Nuestra Habitación Superior son reconocidas como las más amplias y confortables de su categoría en la zona centro de la ciudad. En Hotel Adhara Cancún brindamos un equipamiento moderno, con una gran variedad de servicios para sus estancias de trabajo o descanso.";

$_GLOBALS['disponibilidad-pago-destino'] = "Pago en Hotel";
$_GLOBALS['disponibilidad-room-close'] = "Fecha cerrada:";
$_GLOBALS['disponibilidad-room-total'] = "Total por noche(s):";
$_GLOBALS['disponibilidad-room-currency'] = "MXN";
$_GLOBALS['disponibilidad-room-only-adults'] = "Solo adultos";
$_GLOBALS['disponibilidad-room-capacity'] = "Se excede la capacidad del cuarto";
$_GLOBALS['disponibilidad-room-max-capacity'] = "Capacidad Maxima por cuarto: ";
$_GLOBALS['disponibilidad-room-no-aviable'] = "Cuarto no disponible/Ocupación al Máximo";
$_GLOBALS['disponibilidad-room-no-aviable-date'] = "Para la(s) fecha(s):";
$_GLOBALS['disponibilidad-room-price-clubestrella'] = "Precio Clubestrella:";
$_GLOBALS['disponibilidad-room-promocion'] = "Promoción";
$_GLOBALS['disponibilidad-room-button'] = "Reservar ";
$_GLOBALS['disponibilidad-room-grupos'] = "Grupos";

/* Página del formulario de Reserva */
$_GLOBALS['reserva-h1'] = "Datos para el Hotel";
$_GLOBALS['reserva-rooms'] = "Habitación";
$_GLOBALS['reserva-adults'] = "Adultos:";
$_GLOBALS['reserva-kids'] = "Niños:";
$_GLOBALS['reserva-nights'] = "Noche(s):";
$_GLOBALS['reserva-room-type'] = "Tipo de Habitación: ";
$_GLOBALS['reserva-total-pay'] = "Total:";
$_GLOBALS['reserva-include-tax'] = "(incluye impuestos)";
$_GLOBALS['reserva-currency'] = "MXN";
$_GLOBALS['reserva-promotion-month'] = "Promoción del mes de ";
$_GLOBALS['clubestrella-label'] = "¿Eres miembro Club Estrella?";
$_GLOBALS['clubestrella-h'] = "CLUB ESTRELLA";
$_GLOBALS['clubestrella-p'] = 'Te invitamos a unirte a nuestro programa de recompensas para el viajero frecuente, creado para reconocer su preferencia mediante beneficios exclusivos.';
$_GLOBALS['reserva-clubestrella-member'] = "Inicia Sesión";
$_GLOBALS['reserva-clubestrella-new-member'] = "Soy Nuevo";
$_GLOBALS['reserva-clubestrella-recover'] = "Recuperar <br> Password";
$_GLOBALS['reserva-text'] = "Llegando:";
$_GLOBALS['reserva-text-2'] = " de ";
$_GLOBALS['reserva-text-3'] = " del ";
$_GLOBALS['reserva-text-4'] = " Saliendo:";
$_GLOBALS['reserva-clubestrella-name'] = "Nombre(s):";
$_GLOBALS['reserva-clubestrella-lastname'] = "Apellido(s):";
$_GLOBALS['reserva-clubestrella-email'] = "Correo electrónico:";
$_GLOBALS['reserva-clubestrella-email-label'] = "No compartiremos su email a nadie.";
$_GLOBALS['reserva-clubestrella-email-confirm'] = "Confirmar correo electrónico:";
$_GLOBALS['reserva-clubestrella-city'] = "Ciudad:";
$_GLOBALS['reserva-clubestrella-state'] = "Estado/Región:";
$_GLOBALS['reserva-clubestrella-country'] = "País:";
$_GLOBALS['reserva-clubestrella-numero'] = "Número de contacto:";
$_GLOBALS['reserva-clubestrella-comments'] = "Comentarios adicionales:";
$_GLOBALS['reserva-terms'] = "He leído y estoy de acuerdo con los <button type='button' class='btn terms-room' data-toggle='modal' data-target='#myModal'>términos y condiciones</button> de esta reservación.";
$_GLOBALS['reserva-terms-2'] = "Entiendo que será requerido una identificación oficial o la tarjeta de crédito utilizada en esta reservación al momento del registro.";
$_GLOBALS['reserva-pay-bank'] = "Depositar antes de 74 horas";
$_GLOBALS['reserva-pay-bank2'] = "Depósito Bancario";
$_GLOBALS['reserva-pay-hotel'] = "Paga en el Hotel";
$_GLOBALS['reserva-pay-button'] = "Reservar";

$_GLOBALS['reserva-clubestrella-sesion'] = "Inicia Sesión";
$_GLOBALS['reserva-clubestrella-logoff'] = "Cierra Sesión";
$_GLOBALS['reserva-clubestrella-password'] = "Contraseña";
$_GLOBALS['reserva-clubestrella-password-confirm'] = "Confirmar contraseña";
$_GLOBALS['reserva-clubestrella-signup'] = "Registrate en Clubestrella";
$_GLOBALS['reserva-clubestrella-nickname'] = "Apodo";
$_GLOBALS['reserva-clubestrella-company'] = "Empresa";
$_GLOBALS['reserva-clubestrella-phone'] = "Teléfono";
$_GLOBALS['reserva-clubestrella-cp'] = "Código postal";
$_GLOBALS['reserva-clubestrella-address'] = "Dirección";
$_GLOBALS['reserva-clubestrella-login'] = "Ingresar";
$_GLOBALS['reserva-clubestrella-login2'] = "Registrar";
$_GLOBALS['reserva-clubestrella-login3'] = "Enviar";
$_GLOBALS['reserva-clubestrella-cancel'] = "Cancelar";
$_GLOBALS['reserva-clubestrella-recover-text'] = "Recupera tu contraseña";
$_GLOBALS['clubestrella-welcome'] = "Bienvenido ";

/*Página habitaciones */
$_GLOBALS['rooms-h1'] = "Habitaciones en el Hotel Adhara Cancún";
$_GLOBALS['rooms-p'] = "Las habitaciones de Hotel Adhara Cancún son las más amplias y confortables de su categoría, equipadas con modernos servicios para estancias de trabajo o descanso.";
$_GLOBALS['rooms-p2'] = "La arquitectura e interiorismo estilo hacienda mexicana, se presentan de una manera atractiva y son acordes a la modernidad de los servicios del hotel, ofreciendo a nuestros huéspedes la experiencia de vivir su estancia en un México de actualidad y vanguardia.";
$_GLOBALS['room-estandar-h'] = "Habitación Estándar";
$_GLOBALS['room-estandar-p'] = "TV LCD 32";
$_GLOBALS['room-estandar-p2'] = "Aire acondicionado";
$_GLOBALS['room-estandar-p3'] = "2 camas matrimoniales o 1 cama King size";
$_GLOBALS['room-estandar-p4'] = "Wifi gratuito";
$_GLOBALS['room-estandar-p5'] = "Caja de Seguridad";
$_GLOBALS['room-estandar-p6'] = "Traslado a la Playa";
$_GLOBALS['room-estandar-p7'] = "Kit de Planchado";
$_GLOBALS['room-estandar-p8'] = "Radio reloj despertador";
$_GLOBALS['room-estandar-p99'] = "Cafetera";
$_GLOBALS['room-estandar-p9'] = "Cafetera de Cartucho";
$_GLOBALS['room-estandar-p10'] = "Secadora de Cabello";
$_GLOBALS['room-estandar-p11'] = "Espejo";
$_GLOBALS['room-estandar-p12'] = "Escritorio de trabajo";
$_GLOBALS['room-superior-h'] = "Habitación Superior";
$_GLOBALS['room-superior-p'] = "1 cama King size";
$_GLOBALS['room-superior-p2'] = "Teléfono Inalambrico";
$_GLOBALS['room-ejecutivo-h'] = "Habitación Ejecutivo";
$_GLOBALS['room-ejecutivo-p'] = "TV LCD 42";
$_GLOBALS['room-ejecutivo-p2'] = "Cafetera Dolce Gusto";
$_GLOBALS['room-ejecutivo-p3'] = "Batas de baño";
$_GLOBALS['room-ejecutivo-p4'] = "Pantuflas";
$_GLOBALS['room-ejecutivo-p5'] = "Almohadas de memoria";
$_GLOBALS['room-ejecutivo-p6'] = "Colchón ortopédico";
$_GLOBALS['room-ejecutivo-p7'] = "Mini refri";
$_GLOBALS['room-ejecutivo-p8'] = "Microondas";
$_GLOBALS['room-ejecutivo-p9'] = "Tarja";

/* Página Centro de negocios */
$_GLOBALS['bussiness-h1'] = "CENTRO DE NEGOCIOS";
$_GLOBALS['bussiness-p'] = "Un beneficio importante para los viajeros de negocios dentro del hotel Adhara Cancún, es el centro de negocios, que hemos diseñado como un espacio amplio y cómodo, ideal para poder consultar el internet.";
$_GLOBALS['bussiness-p2'] = "Una sala cómoda en el piso ejecutivo, ambientada y decorada con mobiliario de lujo, teniendo a disposición de los usuarios una gran pantalla de alta resolución para mostrar videos, fotos y presentaciones multimedia.";
$_GLOBALS['bussiness-h2'] = "ÁREA DE TRABAJO & CONEXIÓN A INTERNET";
$_GLOBALS['bussiness-p3'] = "Ponemos a disposición de los viajeros de negocios, un espacio ideal para la productividad empresarial, contando con mesa de trabajo y varias computadoras con conexión permanente a internet.";
$_GLOBALS['bussiness-p4'] = "Una sala cómoda en el piso ejecutivo, ambientada y decorada con mobiliario de lujo, teniendo a disposición de los usuarios una gran pantalla de alta resolución para mostrar videos, fotos y presentaciones multimedia.";

/* Página de la alberca */
$_GLOBALS['alberca-h'] = "ALBERCA & POOL BAR";
$_GLOBALS['alberca-p'] = "Uno de los lugares más agradables del Hotel Adhara Cancún son sus instalaciones de alberca y pool bar.";
$_GLOBALS['alberca-p2'] = "Un espacio refrescante, ideal para relajarse acostado en un camastro bajo el sol, con una bebida tropical	en la mano, preparada al gusto de los visitantes.";
$_GLOBALS['alberca-p3'] = "Un espacio envuelto por una arquitectura y jardinería muy atractivas, con áreas que proyectan las sombras de las altas palmeras reales, en su relajante vaivén provocado por el agradable viento caribeño que ahí se deja sentir todos los días.";
$_GLOBALS['alberca-h1'] = "Pool Grill";
$_GLOBALS['alberca-p4'] = "En un espacio adecuado dentro del área de alberca, colocamos una parrilla para poder ofrecer a nuestros visitantes, en ciertos días, algunas delicias preparadas al carbón.";
$_GLOBALS['alberca-p5'] = "Aromas y sabores de antojo, muy aplaudidos por los huéspedes que han decidido pasar un momento de su viaje muy agradable dentro de nuestras instalaciones.";

/* Página de contacto */
$_GLOBALS['contacto-h'] = "CONTÁCTANOS";
$_GLOBALS['contacto-h2'] = "Encuentranos en:";
$_GLOBALS['contacto-p'] = "Av. Carlos Nader 1,2,3 SM.1,MZ.2, Cancún.";
$_GLOBALS['contacto-p1'] = "Quintana Roo, México, CP.77500.";
$_GLOBALS['contacto-p2'] = "Llamando sin costo al: 01 800 711-15-31 (México).";
$_GLOBALS['contacto-p3'] = "Teléfono: +52 (998) 881 65 00";
$_GLOBALS['contacto-p4'] = "Fax: +52 (998) 884 83 76";
$_GLOBALS['contacto-p5'] = "reservaciones@gphoteles.com";
$_GLOBALS['contacto-p6'] = "grupos@gphoteles.com";
$_GLOBALS['contacto-h2'] = "Ubicación Privilegiada";
$_GLOBALS['contacto-p11'] = "Av. Carlos Nader 1,2,3 SM.1,MZ.2";
$_GLOBALS['contacto-p12'] = "Cancún Quintana Roo, México,";
$_GLOBALS['contacto-p13'] = "CP: 77500";

$_GLOBALS['contacto-p7'] = "El hotel Adhara Cancún está en el centro de la ciudad de Cancún, donde se desarrolla su actividad económica, con fácil acceso a las avenidas Tulum y Bonampak, principales vías de entrada a la ciudad.";
$_GLOBALS['contacto-p8'] = "Se localiza a 25 minutos del aeropuerto, a 5 minutos de los centros comerciales: plaza Las Américas, plaza Malecón las Américas y Malecón Cancún.";
$_GLOBALS['contacto-p9'] = "Desde el hotel Adhara la estación de autobuses está a 600 metros, el embarcadero de Cancún a 5 kilómetros, y el embarcadero de Puerto Juárez para cruzar a Isla Mujeres a 3 kilómetros.";
$_GLOBALS['contacto-p10'] = "Desde el hotel Adhara se tiene cercanía a la plaza del ayuntamiento, al parque de las palapas, a los mercados 23 y 28, al mercado de artesanías en la avenida Tulum, instituciones bancarias, farmacias, tiendas de servicios o bien a la amplia gastronomía internacional con restaurantes especializados en comida mexicana, argentina, italiana, entre otros.";
$_GLOBALS['contacto-img'] = "img/contacto/map.png";

$_GLOBALS['contacto-whats'] = "img/contacto/whats.png";

/* Página de eventos */
$_GLOBALS['eventos-h'] = "EVENTOS GRUPOS Y CONVENCIONES";
$_GLOBALS['eventos-h2'] = "¿quiénes somos?";
$_GLOBALS['eventos-h3'] = "eventos adhara";
$_GLOBALS['eventos-h4'] = "catering";
$_GLOBALS['eventos-h5'] = "instalaciones";
$_GLOBALS['eventos-h6'] = "menú";
$_GLOBALS['eventos-p'] = "Lo invitamos a concertar una cita para hacernos una visita, a modo de conocer nuestros espacios y las facilidades de servicio que podemos ofrecerle, para que su próximo evento se convierta en una nueva historia de éxito.";
$_GLOBALS['eventos-p2'] = "Otras opciones para realizar tu evento:";
$_GLOBALS['eventos-name'] = "Nombre";
$_GLOBALS['eventos-fecha'] = "Fecha Evento";
$_GLOBALS['eventos-pax'] = "Número de Personas";
$_GLOBALS['eventos-telefono'] = "Teléfono";
$_GLOBALS['eventos-email'] = "Email";
$_GLOBALS['eventos-tipo'] = "Tipo de Evento";
$_GLOBALS['eventos-servicios'] = "Servicios Requeridos:";
$_GLOBALS['eventos-servicios-label'] = "Selecciona las opciones deseadas";
$_GLOBALS['eventos-servicios-h'] = "Servicios";
$_GLOBALS['eventos-servicios-p'] = "Audio";
$_GLOBALS['eventos-servicios-p2'] = "Pantalla";
$_GLOBALS['eventos-servicios-p3'] = "Proyector";
$_GLOBALS['eventos-servicios-p4'] = "Tarima";
$_GLOBALS['eventos-servicios-p5'] = "Rotafolio";
$_GLOBALS['eventos-servicios-p6'] = "Alimentos y Bebidas";
$_GLOBALS['eventos-servicios-p7'] = "Desayuno";
$_GLOBALS['eventos-servicios-p8'] = "Estación de Café";
$_GLOBALS['eventos-servicios-p9'] = "Comida";
$_GLOBALS['eventos-servicios-p10'] = "Canapes";
$_GLOBALS['eventos-servicios-p11'] = "Cena";
$_GLOBALS['eventos-servicios-p12'] = "Comentarios";
$_GLOBALS['eventos-alberca'] = "Alberca";
$_GLOBALS['eventos-jardin'] = "Jardín";
$_GLOBALS['eventos-rest'] = "Restaurante";
$_GLOBALS['eventos-juntas'] = "Sala de juntas";
$_GLOBALS['eventos-salones'] = "Salones";
$_GLOBALS['eventos-send'] = "Enviar";
$_GLOBALS['eventos-whats'] = "img/eventos/whatsapp.png";

$_GLOBALS['eventos-sec1'] = "Desde nuestros inicios hemos sido pioneros realizando eventos en Cancún, mejorando constantemente nuestro servicio de hotelería a lo largo de 28 años haciendo magia en cada detalle.";
$_GLOBALS['eventos-sec2'] = "Somos una empresa líder en el servicio de catering asesoría y planeación de los eventos más importantes en Cancún Quintana Roo. Ofreciendo a todos sus clientes experiencias gastronómicas únicas y un servicio de exelencia con más de 28 años de experiencia.";
$_GLOBALS['eventos-sec3'] = "La experiencia de Adhara eventos nos permite dar servicio los 365 días del año a nuestros clientes, otorgando servicio de alimentos, equipo, operadores de cocinas, meseros, choferes, capitanes, supervisores y líderes de proyectos.";
$_GLOBALS['eventos-sec4'] = "Conoce nuestra variedad de platillos realizado por nuestro chef, deleita a tus invitados con opciones para acompañar así como postres y canapés.";
$_GLOBALS['eventos-sec5'] = "Contamos con amplios salones para tus eventos, diferentes espacios para cualquier tipo de evento y un hermoso jardín para realizar eventos al aire libre.";
$_GLOBALS['eventos-sec6'] = "Ven y conoce nuestros espacios.";
$_GLOBALS['eventos-img'] = "img/eventos/events.png";
$_GLOBALS['eventos-img2'] = "img/eventos/events_1.png";

$_GLOBALS['eventos-salones-mob'] = "img/eventos/salones_mob_esp.png";


/* Página de gimnasio */
$_GLOBALS['gym-h'] = "GIMNASIO";
$_GLOBALS['gym-p'] = "Pensando en nuestros huéspedes de negocios y vacacionistas, una área climatizada para ejercitarse en cualquier momento del día sin salir de las instalaciones.";

/* Página del lobby */
$_GLOBALS['lobby-h'] = "LOBBY BAR";
$_GLOBALS['lobby-p'] = "El área de recepción del Hotel Adhara Cancún, es un espacio muy cómodo, fresco y agradable, con una arquitectura circular muy distintiva, donde convergen los huéspedes en diferentes momentos de su estancia.";
$_GLOBALS['lobby-p2'] = "Dentro del lobby, el espacio diseñado para el bar, es un lugar que invita a los viajeros a tomar un momento de descanso para refrescarse con la bebida de su gusto y antojo.";
$_GLOBALS['lobby-p3'] = "Cualquier hora del día es perfecta para pasar un momento muy agradable dentro de este emblemático espacio del hotel que es ideal para la convivencia, el romance o las conversaciones de negocios.";

/* Página de Somos */
$_GLOBALS['somos-h'] = "Somos la mejor opción para";
$_GLOBALS['somos-h2'] = "hospedarse en la ciudad de Cancún";
$_GLOBALS['somos-p'] = "Ofrecemos el servicio hotelero más completo y de mayor calidad en categoría 4 estrellas en el centro de Cancún.";

$_GLOBALS['somos-p2'] = "Nuestra promesa es una estancia cómoda y placentera durante su viaje de vacaciones o negocios, porque contamos con servicios de alta calidad, dentro de espacios diseñados bajo un atractivo estilo arquitectónico colonial mexicano.";
$_GLOBALS['somos-p3'] = "La privilegiada ubicación del hotel sobre la avenida Nader primer avenida histórica de la ciudad de Cancún permite a los huéspedes tener proximidad a los atractivos y servicios más importantes de la ciudad, ofreciendo simultáneamente el acceso rápido a las mejores playas de Cancún e Isla Mujeres.";
$_GLOBALS['somos-p4'] = "<strong>“Hacer magia en cada detalle”</strong> es el enfoque y filosofía de nuestro equipo de colaboradores, que se esfuerzan día con día para ofrecer una atención eficiente, cálida y personalizada.";
$_GLOBALS['somos-p5'] = "Nuestros precios son los más competitivos.";
$_GLOBALS['somos-p6'] = "La incorporación de los consumos de nuestros húespedes a través del programa de lealtad “Club Estrella”, nos permite ofrecer un valor agregado muy atractivo, otorgando importantes beneficios bajo la cobertura del sistema de OK-Trip en gran parte de la República Mexicana.";
$_GLOBALS['somos-img'] = "img/somos/cupula.png";

/* Página de traslados */
$_GLOBALS['traslado-h'] = "Traslado Aeropuerto y Playa";
$_GLOBALS['traslado-p'] = "*Siendo huésped de nuestro hotel, usted cuenta con el servicio de transportación.";
$_GLOBALS['traslado-p2'] = "Nota: Este servicio está sujeto a disponibilidad y horario, aplican restricciones, reserva con nosotros y agenda tu transporte con anticipación.";
$_GLOBALS['traslado-img'] = "img/traslado-esp.png";

/* Errores labels */
$_GLOBALS['defaultD'] = "Fechas no disponibles.";

/* Restaurante  */
$_GLOBALS['restaurante-h'] = "NUESTRO RESTAURANTE";
$_GLOBALS['restaurante-p'] = "Somos un restaurante artesanal por que hacemos todo desde el principio y de una forma muy personal, con dedicacion y trabajo diario.";
$_GLOBALS['restaurante-p2'] = "Conoce nuestro restaurante y degusta de la variedad de platillos con el toque mexicano que nos distingue. Disfruta también de nuestra variedad de postres y pan hecho en casa.";
$_GLOBALS['restaurante-img'] = "img/restaurante/adharaGrill-esp.jpg";
$_GLOBALS['restaurante-img2'] = "img/services/restaurant/banner_5.png";
$_GLOBALS['restaurante-img3'] = "img/services/restaurant/banner_3.png";

$_GLOBALS['restaurante-postre'] = "Disfruta también de nuestra variedad de postres y pan hecho en casa.";

$_GLOBALS['restaurante-menu-mob'] = "img/services/restaurant/menu_mob.png";

/* Cocodrillos */
$_GLOBALS['cocodrillos-img'] = "img/restaurante/cocodrillos-esp.jpg";
$_GLOBALS['cocodrillos-bann'] = "img/services/cocodrillos/banner.png";
$_GLOBALS['cocodrillos-bann-mob'] = "img/services/cocodrillos/banner_mob.png";

/* Grupos */
$_GLOBALS['grupos-h'] = "GRUPOS";
$_GLOBALS['grupos_tittle'] = "¿Necesitas 10 habitaciones o más por noche?";
$_GLOBALS['grupos_label'] = "Reserva con nosotros tarifa especial para grupos, en donde te brindaremos los siguientes beneficios:";
$_GLOBALS['grupos_l1'] = "La tarifa de habitación más baja GARANTIZADA";
$_GLOBALS['grupos_l2'] = "Un asesor especializado que te guiará antes, durante y después de tu estancia en nuestro hotel.";
$_GLOBALS['grupos_l3'] = "Precios especiales en alimentos para las personas hospedadas.";
$_GLOBALS['grupos_l4'] = "Tarifas especiales en Tours.";
$_GLOBALS['grupos_l5'] = "Habitaciones gratis para el coordinador*";
$_GLOBALS['grupos_recommend'] = "Puntos club estrella por cada habitación que podrás utilizar para canjear por diferentes artículos o noches de hospedaje **";
$_GLOBALS['grupos_p1'] = "*1 habitación sin alimentos, por cada 20 hab pagadas por noche";
$_GLOBALS['grupos_p2'] = "**Los puntos se podrán canjear en nuestra página";
$_GLOBALS['grupos_link'] = "www.clubestrella.mx";
$_GLOBALS['grupos_p3'] = "o en nuestra agencia de viajes ok Trip. (Aplican restricciones)";

$_GLOBALS['grupos_whats'] = "img/grupos/whats_es.png";
$_GLOBALS['grupos_whatsapp'] = "img/grupos/whatsapp.png";
$_GLOBALS['grupos_img'] = "img/grupos/info.png";


$_GLOBALS['404-p'] = "LA PAGINA <br>QUE BUSCAS";
$_GLOBALS['404-p2'] = "NO";
$_GLOBALS['404-p3'] = "SE ENCUENTRA <br> DISPONIBLE";



/* Privacidad */

$_GLOBALS['footer-privacy'] = "Aviso de Privacidad";
$_GLOBALS['footer-terms'] = "Terminos y Condiciones";


$_GLOBALS['shuttle-p'] = "Al termino de tu estancia te llevamos al aeropuerto.";
$_GLOBALS['shuttle-img'] = "/img/services/shuttle/shuttle_.png";

$_GLOBALS['club-img'] = "img/places/estrella.png";


$_GLOBALS['foot-p'] = "Hotel Adhara Cancún pertenece a:";
$_GLOBALS['home-image'] = "img/home.png";

$_GLOBALS['somos-details'] = "Hacer magia en cada detalle";
$_GLOBALS['somos-po'] = "Es el enfoque y filosofía de nuestro equipo de colaboradores, que se esfuerzan día con día para ofrecer una atención eficiente, cálida y personalizada.";


/* View Club Estrella */

$_GLOBALS['clubestrella-img'] = "img/clubestrella/card.png";
$_GLOBALS['clubestrella-img2'] = "img/clubestrella/subscribe.png";
$_GLOBALS['clubestrella-img3'] = "img/clubestrella/items.png";
$_GLOBALS['clubestrella-img4'] = "img/clubestrella/btn_subscribe.png";

$_GLOBALS['clubestrella-h'] = "TE INVITAMOS A SER PARTE <br> DE NUESTRO";
$_GLOBALS['clubestrella-h2'] = "CLUB DE LEALTAD";
$_GLOBALS['clubestrella-h3'] = "ACUMULA PUNTOS";
$_GLOBALS['clubestrella-h4'] = "HOSPEDÁNDOTE CON NOSOTROS";
$_GLOBALS['clubestrella-h5'] = "Comienza a sumar puntos comprando con nuestros socios";
$_GLOBALS['clubestrella-h6'] = "Cambia tus puntos por diferentes recompensas:";

$_GLOBALS['img-mob-1'] = "img/clubestrella/card_esp_mob.png";
$_GLOBALS['img-mob-2'] = "img/clubestrella/subscribe_esp_mob.png";
$_GLOBALS['img-mob-3'] = "img/clubestrella/.png";
$_GLOBALS['img-mob-4'] = "img/clubestrella/item_en_mob.png";


/*Vista error cuartos disponibles */

$_GLOBALS['error-p'] = "La habitación";
$_GLOBALS['error-p2'] = "ya no cuenta con";
$_GLOBALS['error-p3'] = "cuartos disponibles.";
$_GLOBALS['error-p4'] = "Sentimos los";
$_GLOBALS['error-p5'] = "inconvenientes";

?>