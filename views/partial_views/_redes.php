<ul class='social'>
  <li>
	<a href="https://www.facebook.com/HotelAdharaCancun" target="_blank"><i class="fab fa-facebook-f"></i></a>
  </li>
  <li>
	<a href="https://twitter.com/adharacancun" target="_blank" ><i class="fab fa-twitter"></i></a>
  </li>
  <li>
	<a href="https://www.tripadvisor.com.mx/Hotel_Review-g150807-d154412-Reviews-Adhara_Hacienda_Cancun-Cancun_Yucatan_Peninsula.html" target="_blank"><i class="fab fa-tripadvisor"></i></a>
  </li>
  <li>
	<a href="http://adhara-facturacion.arpon.com/" target="_blank"><i class="far fa-file-archive"></i></a>
  </li>
</ul>