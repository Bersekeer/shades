<?php 

$_GLOBALS['lng'] = 1;

/* Sección del header */
$_GLOBALS['header_home'] = "HOME";
$_GLOBALS['header_somos'] = "ABOUT US";
$_GLOBALS['header_rooms'] = "ROOMS";
$_GLOBALS['header_servicios'] = "SERVICES";
$_GLOBALS['header_alberca'] = "POOL";
$_GLOBALS['header_playa'] = "TRANSPORTATION";
$_GLOBALS['header_restaurante'] = "ADHARA GRILL";
$_GLOBALS['header_cocodrillo'] = "COCODRILLOS";
$_GLOBALS['header_bussiness'] = "BUSSINESS CENTER";
$_GLOBALS['header_lobby'] = "LOBBY BAR";
$_GLOBALS['header_gym'] = "GYM";
$_GLOBALS['header_eventos'] = "EVENTS";
$_GLOBALS['header_contacto'] = "CONTACT";
$_GLOBALS['header_grupos'] = "GROUPS";
$_GLOBALS['header_meet_us'] = "MEET US";
$_GLOBALS['header_whats'] = "/img/whatsapp-en.png";
$_GLOBALS['keywords']= "Official  site, hotel rooms cancún, hotel reservations cancun, discount hotel cancun, vacations  packages cancun, vacations in  cancun, cheap hotel cancun, hotel deals cancun, book  the  best price in  cancun, search  & book  hotels cancun, lowest price guaranteed, book your hotel Adhara and  save, compare price and  save on  your  next  stay, pay when you stay,free Wifi, book  the  best current deals  online,  find  cheap hotels  in  cancun, hot sale 2019, buen  fin  2019 ";

$_GLOBALS['flag_src'] = "/img/flags/mex.png";
$_GLOBALS['flag_alt'] = "es";

/* Sección del Buscador */
$_GLOBALS['search-from'] = "Check In";
$_GLOBALS['search-to'] = "Check Out";
$_GLOBALS['search-rooms'] = "Rooms";
$_GLOBALS['search-code'] = "Special Rate";

$_GLOBALS['search-dates'] = "Booking dates";
$_GLOBALS['search-room'] = "Room";
$_GLOBALS['search-enter'] = "Search";
$_GLOBALS['search-apply'] = "Apply";
$_GLOBALS['search-adults'] = "Adults";
$_GLOBALS['search-kids'] = "Kids";
$_GLOBALS['search-delete'] = "delete";

/* Seccion de Tarifa del Dia */
$_GLOBALS['rates-taxes'] = "Taxes included";
$_GLOBALS['rates-whats'] = "Quote,Book and Pay by WhatsApp";

/* Imagen Home */
$_GLOBALS['url-home-800'] = "/img/places/home_en_mob.png";
$_GLOBALS['url-home-500'] = "/img/places/home_en_mobile.png";

/* Quick Search */

$_GLOBALS['quick-date'] = "Today's rate<br>Include breakfast*";
$_GLOBALS['quick-date-mob'] = "Today's rate";
$_GLOBALS['quick-price'] = "$69 USD";
$_GLOBALS['quick-tarifa'] = "BOOK WITH</span> <br>MAGIC RATE";
$_GLOBALS['quick-whats'] = "Quote,Book and Pay<br> by Whatsapp";
$_GLOBALS['quick-style'] = "margin-left:18px;";
$_GLOBALS['quick-font'] = "font-size:16px;";
$_GLOBALS['quick-offer'] = "Free breakfast up to 2 people";

/* Página Home */

$_GLOBALS['home-tittle-1'] = "Welcome to";
$_GLOBALS['home-tittle-2'] = "Adhara Cancun Hotel";
$_GLOBALS['home-descrip'] = "Adhara Cancun Hotel is located in Downtown Cancun, its great location is very convenient for both, leisure and business travelers, we are known for the warmth or our personal, highly professional and efficient service.";
$_GLOBALS['home-descrip2'] = "The colonial style architecture and interior design make the hotel an ideal place to spend a few days in a very comfortable, relaxed and pleasant ambience with the most modern services.";

$_GLOBALS['home-rooms'] = "OUR ROOMS";
$_GLOBALS['home-details'] = "Details";

$_GLOBALS['home-room-estandar'] = "Estandar room";
$_GLOBALS['home-room-junior'] = "Superior room";
$_GLOBALS['home-room-ejecutivo'] = "Executive room";
$_GLOBALS['home-room-descrip'] = "Equipped with modern amenities that are ideal to work or to relax.";
$_GLOBALS['home-room-features'] = "The room has a safety deposit box and a comfortable working desk.";

$_GLOBALS['home-galeria-playa'] = "free shuttle";
$_GLOBALS['home-galeria-interior'] = "Interior & Architecture";
$_GLOBALS['home-galeria-lobby'] = "Lobby & Bar";
$_GLOBALS['home-galeria-restaurante'] = "Adhara Grill restaurant";
$_GLOBALS['home-galeria-negocios'] = "Business center";
$_GLOBALS['home-galeria-juntas'] = "Meeting rooms";
$_GLOBALS['home-galeria-alberca'] = "Pool";
$_GLOBALS['home-galeria-eventos'] = "Events & Conventions";

$_GLOBALS['home-magia-h1'] = "Making magic in every detail!";
$_GLOBALS['home-magia-p'] = "Adhara Hotel’s philosophy is based on human warmth and personalized service for our guests. In keeping with our Mexican style, our service is characterized by a charismatic, friendly, courteous, and overall hospitable spirit, which makes us great hosts on an international level. On a corporate level, we are committed to 'making magic in every detail', this being our motto and objective, we strive to work on offering our valued guests great comfort and a memorable sense of satisfaction.";
$_GLOBALS['home-magia-h2'] = "Get the best rate!";
$_GLOBALS['home-magia-p2'] = "We guarantee the best rate, booking directly with the hotel website you will get the lowest rate in a secure and efficient way.";
$_GLOBALS['home-magia-h3'] = "FREE SHUTTLE";
$_GLOBALS['home-magia-p6'] = "Booking directly with us grants the free transportation to the airport at checking-out subject to  established schedule and availability.";
$_GLOBALS['home-magia-p7'] = "Book in advance and schedule the ride*";
$_GLOBALS['home-magia-p3'] = "Booking directly with us grants the free transportation to the airport at checking-out";
$_GLOBALS['home-magia-p4'] = "subject to  established schedule and availability.";
$_GLOBALS['home-magia-p5'] = "Restrictions apply*";
$_GLOBALS['home-testimonio-h1'] = "Testimonials";
$_GLOBALS['home-return'] = "Home";

/* Página disponibilidad */
$_GLOBALS['room-taxes'] = "All prices include taxes";
$_GLOBALS['room-label'] = "Rooms: ";
$_GLOBALS['room-adults'] = " / Adults: ";
$_GLOBALS['room-kids'] = " / Kids: ";
$_GLOBALS['room-nights'] = " / Nights: ";
$_GLOBALS['room-text'] = "Arriving ";
$_GLOBALS['room-text-2'] = " of ";
$_GLOBALS['room-text-3'] = " of ";
$_GLOBALS['room-text-4'] = " leaving ";

$_GLOBALS['room-feature'] = "Wifi";
$_GLOBALS['room-feature-2'] = "Coffee maker";
$_GLOBALS['room-feature-3'] = "Room service";
$_GLOBALS['room-feature-4'] = "Safety box";
$_GLOBALS['room-feature-5'] = "Office desk";
$_GLOBALS['room-feature-plus'] = "more information";

$_GLOBALS['disponibilidad-room-estandar'] = "Estandar room";
$_GLOBALS['disponibilidad-room-estandar-text'] = "Estas cómodas habitaciones cuentan con un práctico equipamiento que incluye una cama confortable para dormir plenamente, así como televisión de pantalla plana, cable, escritorio, silla ergonómica, así como acceso a Internet WiFi.";
$_GLOBALS['disponibilidad-room-superior'] = "Superior room";
$_GLOBALS['disponibilidad-room-superior-text'] = "Nuestra Habitación Superior son reconocidas como las más amplias y confortables de su categoría en la zona centro de la ciudad. En Hotel Adhara Cancun brindamos un equipamiento moderno, con una gran variedad de servicios para sus estancias de trabajo o descanso.";
$_GLOBALS['disponibilidad-room-ejecutivo'] = "Executive room";
$_GLOBALS['disponibilidad-room-ejecutivo-text'] = "Nuestra Habitación Superior son reconocidas como las más amplias y confortables de su categoría en la zona centro de la ciudad. En Hotel Adhara Cancun brindamos un equipamiento moderno, con una gran variedad de servicios para sus estancias de trabajo o descanso.";

$_GLOBALS['disponibilidad-pago-destino'] = "Pay at Hotel";
$_GLOBALS['disponibilidad-room-close'] = "Close date:";
$_GLOBALS['disponibilidad-room-total'] = "Total for night(s):";
$_GLOBALS['disponibilidad-room-currency'] = "USD";
$_GLOBALS['disponibilidad-room-only-adults'] = "Only Adults";
$_GLOBALS['disponibilidad-room-capacity'] = "Max capacity for the room exceed.";
$_GLOBALS['disponibilidad-room-max-capacity'] = "Max capacity for the room: ";
$_GLOBALS['disponibilidad-room-no-aviable'] = "Room no aviable/Maximum Occupation";
$_GLOBALS['disponibilidad-room-no-aviable-date'] = "For the date(s):";
$_GLOBALS['disponibilidad-room-price-clubestrella'] = "Clubestrella Price:";
$_GLOBALS['disponibilidad-room-promocion'] = "Promotion";
$_GLOBALS['disponibilidad-room-button'] = "Book ";
$_GLOBALS['disponibilidad-room-grupos'] = "Groups";

/* Página del formulario de Reserva */
$_GLOBALS['reserva-h1'] = "Information for the Hotel";
$_GLOBALS['reserva-rooms'] = "Room";
$_GLOBALS['reserva-adults'] = "Adults:";
$_GLOBALS['reserva-kids'] = "Kids:";
$_GLOBALS['reserva-nights'] = "Night(s):";
$_GLOBALS['reserva-room-type'] = "Room type: ";
$_GLOBALS['reserva-total-pay'] = "Total:";
$_GLOBALS['reserva-include-tax'] = "(include taxes)";
$_GLOBALS['reserva-currency'] = "USD";
$_GLOBALS['reserva-promotion-month'] = "Promotion of the month of ";
$_GLOBALS['clubestrella-label'] = "Are you Club Estrella member?";
$_GLOBALS['clubestrella-h'] = "CLUB ESTRELLA";
$_GLOBALS['clubestrella-p'] = 'We invite you to join our loyalty program "Club Estrella" and add points, you can redeem such points for room nights or articles of our list.';
$_GLOBALS['reserva-clubestrella-member'] = "Log In";
$_GLOBALS['reserva-clubestrella-new-member'] = "I'm new";
$_GLOBALS['reserva-clubestrella-recover'] = "Recover password";
$_GLOBALS['reserva-text'] = "Arriving:";
$_GLOBALS['reserva-text-2'] = " of ";
$_GLOBALS['reserva-text-3'] = " of ";
$_GLOBALS['reserva-text-4'] = "Leaving:";
$_GLOBALS['reserva-clubestrella-name'] = "Name(s):";
$_GLOBALS['reserva-clubestrella-lastname'] = "Lastname(s):";
$_GLOBALS['reserva-clubestrella-email'] = "Email:";
$_GLOBALS['reserva-clubestrella-email-label'] = "Well never share your email with anyone else.";
$_GLOBALS['reserva-clubestrella-email-confirm'] = "Confirm email:";
$_GLOBALS['reserva-clubestrella-city'] = "City:";
$_GLOBALS['reserva-clubestrella-state'] = "State:";
$_GLOBALS['reserva-clubestrella-country'] = "Country:";
$_GLOBALS['reserva-clubestrella-numero'] = "Contact number:";
$_GLOBALS['reserva-clubestrella-comments'] = "Message:";
$_GLOBALS['reserva-terms'] = "I have read and accept the <button type='button' class='btn terms-room' data-toggle='modal' data-target='#myModal'>Terms and Conditions and Privacy Policies</button> of Hotel Adhara Cancun.";
$_GLOBALS['reserva-terms-2'] = "I understand i will need to show an official id or the credit card used to make the reservation.";
$_GLOBALS['reserva-pay-bank'] = "Deposit before 74 hrs";
$_GLOBALS['reserva-pay-bank2'] = "Bank Payment";
$_GLOBALS['reserva-pay-hotel'] = "Pay at Hotel";
$_GLOBALS['reserva-pay-button'] = "Book";

$_GLOBALS['reserva-clubestrella-sesion'] = "Login";
$_GLOBALS['reserva-clubestrella-logoff'] = "Sign out";
$_GLOBALS['reserva-clubestrella-password'] = "Password";
$_GLOBALS['reserva-clubestrella-password-confirm'] = "Confirm password";
$_GLOBALS['reserva-clubestrella-signup'] = "Become a Clubestrella member";
$_GLOBALS['reserva-clubestrella-nickname'] = "Username";
$_GLOBALS['reserva-clubestrella-company'] = "Company";
$_GLOBALS['reserva-clubestrella-phone'] = "Phone";
$_GLOBALS['reserva-clubestrella-cp'] = "Postal code";
$_GLOBALS['reserva-clubestrella-address'] = "Address";
$_GLOBALS['reserva-clubestrella-login'] = "Loging";
$_GLOBALS['reserva-clubestrella-login2'] = "Register";
$_GLOBALS['reserva-clubestrella-login3'] = "Submit";
$_GLOBALS['reserva-clubestrella-cancel'] = "Cancel";
$_GLOBALS['reserva-clubestrella-recover-text'] = "Recover password";
$_GLOBALS['clubestrella-welcome'] = "Welcome ";

/*Página habitaciones */
$_GLOBALS['rooms-h1'] = "Hotel Adhara Cancun Rooms";
$_GLOBALS['rooms-p'] = "The hotel Adhara Cancun rooms are the most spacious and comfortable in its category, are equipped with modern amenities for both, working and relaxing.";
$_GLOBALS['rooms-p2'] = "The hotel’s Mexican hacienda style architecture and interior design are presented in an attractive fashion, and are in keeping with our modern hotel services. This offers our guests the experience of enjoying their stay in a Mexico that is both modern and avant-garde.";
$_GLOBALS['room-estandar-h'] = "Standar room";
$_GLOBALS['room-estandar-p'] = "TV LCD 32";
$_GLOBALS['room-estandar-p2'] = "Air conditioning";
$_GLOBALS['room-estandar-p3'] = "1 king-size or 2 double beds";
$_GLOBALS['room-estandar-p4'] = "Free Wifi";
$_GLOBALS['room-estandar-p5'] = "Safety deposit box";
$_GLOBALS['room-estandar-p6'] = "Transportation to the Beach";
$_GLOBALS['room-estandar-p7'] = "Ironing kit";
$_GLOBALS['room-estandar-p8'] = "Radio alarm clock";
$_GLOBALS['room-estandar-p99'] = "Coffee maker";
$_GLOBALS['room-estandar-p9'] = "Cartridge Coffee maker";
$_GLOBALS['room-estandar-p10'] = "Hairdryer";
$_GLOBALS['room-estandar-p11'] = "Vanity mirror";
$_GLOBALS['room-estandar-p12'] = "Working desk";
$_GLOBALS['room-superior-h'] = "Superior room";
$_GLOBALS['room-superior-p'] = "1 king-size bed";
$_GLOBALS['room-superior-p2'] = "Wireless phone";
$_GLOBALS['room-ejecutivo-h'] = "Executive room";
$_GLOBALS['room-ejecutivo-p'] = "TV LCD 42";
$_GLOBALS['room-ejecutivo-p2'] = "Dolce Gusto coffee maker";
$_GLOBALS['room-ejecutivo-p3'] = "Bathrobes";
$_GLOBALS['room-ejecutivo-p4'] = "Slippers";
$_GLOBALS['room-ejecutivo-p5'] = "Memory foam pillows";
$_GLOBALS['room-ejecutivo-p6'] = "Orthopedic mattress";
$_GLOBALS['room-ejecutivo-p7'] = "Frigo bar";
$_GLOBALS['room-ejecutivo-p8'] = "Microwave";
$_GLOBALS['room-ejecutivo-p9'] = "Kitchen sink";

/* Página Centro de negocios */
$_GLOBALS['bussiness-h1'] = "BUSINESS CENTER";
$_GLOBALS['bussiness-p'] = "An important benefit for business travelers in the Adhara Hotel is the business center. We have designed a spacious and comfortable area that is ideal to hold meetings where conversations and successful business presentations can be carried out successfully.";
$_GLOBALS['bussiness-p2'] = "It is a comfortable lounge in the executive floor equipped and decorated with luxury furnishings. A high-resolution screen to display videos, photos and multimedia presentations is available for our guests.";
$_GLOBALS['bussiness-h2'] = "WORKING AREA AND INTERNET CONNECTION";
$_GLOBALS['bussiness-p3'] = "An important benefit for business travelers is the service of the Business  Center.  There is an spacious and comfortable area where you will find computers to assist you to complement  your requirements";
$_GLOBALS['bussiness-p4'] = "A comfortable lounge located on the 4th floor offers the space to watch TV of just a place to read a book.";

/* Página de la alberca */
$_GLOBALS['alberca-h'] = "POOL & POOL BAR";
$_GLOBALS['alberca-p'] = "Some of the most attractive facilities in the hotel are the Adhara pool and the pool bar.";
$_GLOBALS['alberca-p2'] = "It is a cool area that is ideal for relaxing while lying on lounge chair under the sun with a tropical drink in hand, which is prepared to suit the taste of our guests.";
$_GLOBALS['alberca-p3'] = "The area features a very appealing architectural style and landscaping. In certain parts, you can enjoy the shade of the tall royal palms, which slowly sway in the pleasant and relaxing Caribbean wind that can be felt there every day.";
$_GLOBALS['alberca-h1'] = "Pool Grill";
$_GLOBALS['alberca-p4'] = "In a suitable space within the pool area, we have placed a coal grill and, on certain days of the week, we offer our guests grilled delicacies.";
$_GLOBALS['alberca-p5'] = "Alluring aromas and flavors to please our guests who have decided to spend unforgettable moments of their trip within our facilities.";

/* Página de contacto */
$_GLOBALS['contacto-h'] = "CONTACT US";
$_GLOBALS['contacto-h2'] = "Find us in:";
$_GLOBALS['contacto-p'] = "Av. Carlos Nader 1,2,3 SM. 1, MZ. 2, Cancun, Quintana Roo, Mexico, CP.77500";
$_GLOBALS['contacto-p2'] = "Calling free at: 01 800 711-15-31 (Mexico)";
$_GLOBALS['contacto-p3'] = "Phone: +52 (998) 881 65 00";
$_GLOBALS['contacto-p4'] = "Fax: +52 (998) 884 83 76";
$_GLOBALS['contacto-p5'] = "reservaciones@gphoteles.com";
$_GLOBALS['contacto-p6'] = "grupos@gphoteles.com";
$_GLOBALS['contacto-h2'] = "Privileged Location";
$_GLOBALS['contacto-p11'] = "Av. Carlos Nader 1,2,3 SM.1,MZ.2";
$_GLOBALS['contacto-p12'] = "Cancun Quintana Roo, Mexico,";
$_GLOBALS['contacto-p13'] = "CP: 77500";
$_GLOBALS['contacto-p7'] = "The Adhara Cancun Hotel is located in downtown Cancun, where the economic activity takes place. It has easy access to the Tulum and Bonampak Avenues, which are the main entrances to the city.";
$_GLOBALS['contacto-p8'] = "It is located 25 minutes from the airport, 5 minutes from the main shopping centers such as Plaza Las Americas, Plaza Malecon Las Americas and Malecon Cancun.";
$_GLOBALS['contacto-p9'] = "Adhara Hotel is just 600 meters (less than 2,000 ft) from bus station, 5 kilometers (about 3 mi) from the Embarcadero Cancun pier, and the Puerto Juarez dock, where you cross to Isla Mujeres, is 3 kilometers (about 2 mi) away.";
$_GLOBALS['contacto-p10'] = "From the Adhara Hotel you are very close to the Town Hall Square, Parque las Palapas, Markets 23 and 28, the craft market on Tulum Avenue, banks, pharmacies, convenience stores, and a broad range of international cuisine options with restaurants specializing in Mexican, Argentinean, and Italian food, among others.";
$_GLOBALS['contacto-img'] = "img/contacto/map_en.png";

$_GLOBALS['contacto-whats'] = "img/contacto/whats_en.png";

/* Página de eventos */
$_GLOBALS['eventos-h'] = "GROUP EVENTS & CONVENTIONS";
$_GLOBALS['eventos-h2'] = "who are we?";
$_GLOBALS['eventos-h3'] = "adhara events";
$_GLOBALS['eventos-h4'] = "catering";
$_GLOBALS['eventos-h5'] = "facilities";
$_GLOBALS['eventos-h6'] = "menu";
$_GLOBALS['eventos-p'] = "We invite you to make an appointment to see our events facilities, and lets us help you to make your next event a successful one.";
$_GLOBALS['eventos-p2'] = "Another option to perform your event:";
$_GLOBALS['eventos-name'] = "Name";
$_GLOBALS['eventos-fecha'] = "Event Day";
$_GLOBALS['eventos-pax'] = "Number of Guests";
$_GLOBALS['eventos-telefono'] = "Phone";
$_GLOBALS['eventos-email'] = "Email";
$_GLOBALS['eventos-tipo'] = "Event Type";
$_GLOBALS['eventos-servicios'] = "Required Services:";
$_GLOBALS['eventos-servicios-label'] = "Select the desired options";
$_GLOBALS['eventos-servicios-h'] = "Services";
$_GLOBALS['eventos-servicios-p'] = "Audio";
$_GLOBALS['eventos-servicios-p2'] = "Screen";
$_GLOBALS['eventos-servicios-p3'] = "Projector";
$_GLOBALS['eventos-servicios-p4'] = "Stage";
$_GLOBALS['eventos-servicios-p5'] = "Flipchart";
$_GLOBALS['eventos-servicios-p6'] = "Food and Drinks";
$_GLOBALS['eventos-servicios-p7'] = "Breakfast";
$_GLOBALS['eventos-servicios-p8'] = "Coffee maker";
$_GLOBALS['eventos-servicios-p9'] = "Meal";
$_GLOBALS['eventos-servicios-p10'] = "Canapes";
$_GLOBALS['eventos-servicios-p11'] = "Dinner";
$_GLOBALS['eventos-servicios-p12'] = "Message";
$_GLOBALS['eventos-alberca'] = "Pool";
$_GLOBALS['eventos-jardin'] = "Garden";
$_GLOBALS['eventos-rest'] = "Restaurant";
$_GLOBALS['eventos-juntas'] = "Meeting Room";
$_GLOBALS['eventos-salones'] = "Salones";
$_GLOBALS['eventos-send'] = "Send";
$_GLOBALS['eventos-whats'] = "Enviar";

$_GLOBALS['eventos-sec1'] = "For the last 28 years we  have been serving events in Cancun, the events office has improved since then and we are now well known in Downtown Cancun to organize  all kind of events, cocktails, coffee breaks, meetings, exhibitions, corporate, leisure";
$_GLOBALS['eventos-sec2'] = "We are a leading company in the catering service, advising and planning the most important events in Cancun Quintana Roo. Offering all its clients unique gastronomic experiences and a service of excellence with more than 28 years of experience.";
$_GLOBALS['eventos-sec3'] = "The experience of Adhara events allows us to provide service 365 days a year to our clients, providing food service, equipment, kitchen operators, waiters, drivers, captains, supervisors and project leaders.";
$_GLOBALS['eventos-sec4'] = "Large menu variety to satisfy all food and beverages requirements";

$_GLOBALS['eventos-sec5'] = "We offer 8 meeting rooms and a beautiful garden for your events.";
$_GLOBALS['eventos-sec6'] = "Come and visit our facilities.";
$_GLOBALS['eventos-img'] = "img/eventos/events_en.png";
$_GLOBALS['eventos-img2'] = "img/eventos/events_1_en.png";

$_GLOBALS['eventos-salones-mob'] = "img/eventos/salones_mob.png";


/* Página de gimnasio */
$_GLOBALS['gym-h'] = "FITNESS CENTER";
$_GLOBALS['gym-p'] = "Considering the needs of our business and leisure travelers, in this climate-controlled area you can exercise at any time of the day without leaving the premises.";

/* Página del lobby */
$_GLOBALS['lobby-h'] = "Lobby Bar";
$_GLOBALS['lobby-p'] = "The reception area of ​​the Adhara Hotel is a very comfortable, cool and pleasant space. It is a spot with a very distinctive circular architecture where guests can meet at different times during their stay.";
$_GLOBALS['lobby-p2'] = "Inside the lobby, the area designed for the bar, is a place that invites travelers like you to take a moment to cool off with a drink of your choice.";
$_GLOBALS['lobby-p3'] = "Any time of day is perfect to spend a very pleasant time in this emblematic area of the hotel that is ideal for hanging out, a romantic moment or a business conversation.";

/* Página de Somos */
$_GLOBALS['somos-h'] = "We are the best option";
$_GLOBALS['somos-h2'] = "to stay in Downtown Cancun!";
$_GLOBALS['somos-p'] = "We offer the most complete 4-star hotel service with the highest quality in Downtown Cancun.";
$_GLOBALS['somos-p2'] = "We promise you a comfortable and pleasant stay during your vacation or business trip while offering you high quality services within the different areas of the hotel’s attractive Mexican colonial architectural style.";
$_GLOBALS['somos-p3'] = "The hotel’s privileged location on Nader Avenue, the first historic avenue of Cancun allows guests to be in close proximity of major attractions and city services, while offering quick access to the best beaches of Cancun and Isla Mujeres.";
$_GLOBALS['somos-p4'] = "<strong>“Making magic in every detail”</strong> It is our philosophy and our team focus in offering warmth and personal attention.";
$_GLOBALS['somos-p5'] = "Competitive prices";
$_GLOBALS['somos-p6'] = "And by including our guests’ food and beverages with the “Club Estrella” program, it allows us to offer a very attractive added value while providing significant benefits under the OK-Trip system that has coverage in many parts of Mexico.";
$_GLOBALS['somos-img'] = "img/somos/cupula_en.png";

/* Página de traslados */
$_GLOBALS['traslado-h'] = "Transportation to the Airport and Beach";
$_GLOBALS['traslado-p'] = "*As our guest, you have the transportation service.";
$_GLOBALS['traslado-p2'] = "Note: This service is subject to availability, apply restrictions.";
$_GLOBALS['traslado-img'] = "img/traslado-en.png";


/* Errores labels */
$_GLOBALS['defaultD'] = "Dates no Aviable.";

/* Restaurante  */
$_GLOBALS['restaurante-h'] = "OUR RESTAURANT";
$_GLOBALS['restaurante-p'] = "Come and visit our restaurant and  taste our menu with an extraordinary Mexican Flavor.  Enjoy our assorted desserts and sweet rolls homemade ";
$_GLOBALS['restaurante-p2'] = "Come and visit our restaurant and  taste our menu with an extraordinary Mexican Flavor.  Enjoy our assorted desserts and sweet rolls homemade";
$_GLOBALS['restaurante-img'] = "img/restaurante/adharaGrill-en.jpg";
$_GLOBALS['restaurante-img2'] = "img/services/restaurant/banner_5_en.png";
$_GLOBALS['restaurante-img3'] = "img/services/restaurant/banner_3_en.png";

$_GLOBALS['restaurante-menu-mob'] = "img/services/restaurant/menu_mob_en.png";

$_GLOBALS['restaurante-postre'] = "Enjoy the variety of our desserts and home bread.";

/* Cocodrillos */
$_GLOBALS['cocodrillos-img'] = "img/restaurante/cocodrillos-en.jpg";
$_GLOBALS['cocodrillos-bann'] = "img/services/cocodrillos/banner_en.png";
$_GLOBALS['cocodrillos-bann-mob'] = "img/services/cocodrillos/banner_mob_en.png";

/* Grupos */
$_GLOBALS['grupos-h'] = "GROUPS";
$_GLOBALS['grupos_tittle'] = "You need 10 rooms per night , or more?";
$_GLOBALS['grupos_label'] = "Book with us and get the following benefits:";
$_GLOBALS['grupos_l1'] = "The lowest rate possible GUARANTEED.";
$_GLOBALS['grupos_l2'] = "A specialized agent, before, during, and after your stay with us.";
$_GLOBALS['grupos_l3'] = "Special food prices for your group.";
$_GLOBALS['grupos_l4'] = "Special tours rates , booking by our ok travel agency.";
$_GLOBALS['grupos_l5'] = "Free room nights for the group leader.*";
$_GLOBALS['grupos_recommend'] = "Club estrella rewards for every room.**";
$_GLOBALS['grupos_p1'] = "1free room night for every 20 paid rooms per night.";
$_GLOBALS['grupos_p2'] = "**Club estrella rewards can be reedemed on our web  page";
$_GLOBALS['grupos_link'] = "www.clubestrella.mx";
$_GLOBALS['grupos_p3'] = "";
$_GLOBALS['grupos_whats'] = "img/grupos/whats_en.png";
$_GLOBALS['grupos_whatsapp'] = "img/grupos/whats_en.png";
$_GLOBALS['grupos_img'] = "img/grupos/grupos_en.png";

$_GLOBALS['404-p'] = "THE PAGE <br>YOU LOOKING";
$_GLOBALS['404-p2'] = "FOR";
$_GLOBALS['404-p3'] = "IS NOT <br> AVIABLE";


/* Privacidad */

$_GLOBALS['footer-privacy'] = "Notice of Privacy";
$_GLOBALS['footer-terms'] = "Terms and Conditions";

$_GLOBALS['shuttle-p'] = "On check-out use the free shuttle service to go to the airport.";
$_GLOBALS['shuttle-img'] = "/img/services/shuttle/shuttle_en.png";

$_GLOBALS['club-img'] = "img/places/estrella_en.png";

$_GLOBALS['foot-p'] = "Hotel Adhara Cancun is part of:";
$_GLOBALS['home-image'] = "img/home_en.png";

$_GLOBALS['somos-details'] = "Making magic in every detail";
$_GLOBALS['somos-po'] = "It is our philosophy and our team focus in offering warmth and personal attention";


/* View Club Estrella */

$_GLOBALS['clubestrella-img'] = "img/clubestrella/card_en.png";
$_GLOBALS['clubestrella-img2'] = "img/clubestrella/subscribe_en.png";
$_GLOBALS['clubestrella-img3'] = "img/clubestrella/items_en.png";
$_GLOBALS['clubestrella-img4'] = "img/clubestrella/btn_subscribe_en.png";

$_GLOBALS['clubestrella-h'] = "WE INVITE YOU TO JOIN <br> OUR ";
$_GLOBALS['clubestrella-h2'] = "LOYALTY PROGRAM";
$_GLOBALS['clubestrella-h3'] = "ADD POINTS";
$_GLOBALS['clubestrella-h4'] = "LODGING WITH US";
$_GLOBALS['clubestrella-h5'] = "Start earning points by buying with our partners";
$_GLOBALS['clubestrella-h6'] = "Redeem for stay nights and more";


$_GLOBALS['img-mob-1'] = "img/clubestrella/card_en_mob.png";
$_GLOBALS['img-mob-2'] = "img/clubestrella/subscribe_en_mob.png";
$_GLOBALS['img-mob-3'] = "img/clubestrella/.png";
$_GLOBALS['img-mob-4'] = "img/clubestrella/item_en_mob.png";


/*Vista error cuartos disponibles */

$_GLOBALS['error-p'] = "The room";
$_GLOBALS['error-p2'] = "has not";
$_GLOBALS['error-p3'] = "available rooms";
$_GLOBALS['error-p4'] = "Sorry for";
$_GLOBALS['error-p5'] = "the troubles.";

?>