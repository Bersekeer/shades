<div id="gallery_jardin" style="display:none;">

	<a href="#">
	<img alt="Lemon Slice"
	     src="img/eventos/jardin/1.jpg"
	     data-image="img/eventos/jardin/1.jpg"
	     data-description="This is a Lemon Slice"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Peppers"
	     src="img/eventos/jardin/2.jpg"
	     data-image="img/eventos/jardin/2.jpg"
	     data-description="Those are peppers"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Keys"
	     src="img/eventos/jardin/3.jpg"
	     data-image="img/eventos/jardin/3.jpg"
	     data-description="Those are keys"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Friuts in cup"
	     src="img/eventos/jardin/4.jpg"
	     data-image="img/eventos/jardin/4.jpg"
	     data-description="Those are friuts in a cup"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Yellow Flowers"
	     src="img/eventos/jardin/5.jpg"
	     data-image="img/eventos/jardin/5.jpg"
	     data-description="Those are yellow flowers"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Butterfly"
	     src="img/eventos/jardin/6.jpg"
	     data-image="img/eventos/jardin/6.jpg"
	     data-description="This is butterfly"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Boat"
	     src="img/eventos/jardin/7.jpg"
	     data-image="img/eventos/jardin/7.jpg"
	     data-description="This is a boat"
	     style="display:none">
	</a>

	<a href="#">
	<img alt="Woman"
	     src="img/eventos/jardin/8.jpg"
	     data-image="img/eventos/jardin/8.jpg"
	     data-description="This is a woman"
	     style="display:none">
	</a>
		 
</div>