
var qr = "";
var folio = "";
var estuche = "";
var folioFiscal = "";
function myFunction() {
    qr = $("#test").val();
    //$("#demo").innerHTML = "You wrote: " + qr;
    //console.log(qr);
    if(qr.slice(-2) == "¿¿"){

		var arr = qr.split('¿');
		/*console.log(arr[1]);
		console.log(arr[2]);
		console.log(arr[3]);
		console.log(arr[4]);*/
        /*var itemPrice = arr[4].split("/");
        var price = parseFloat(itemPrice[0]);*/
        //console.log(price);
		var item1 = arr[1].split("/");
		folio = item1[0];
		estuche = folio.split("'");
		folioFiscal = estuche[0]+estuche[1]+estuche[2]+estuche[3]+estuche[4];
		console.log(folioFiscal);
		$("#folioFiscal").val(folioFiscal);
		var item2 = arr[2].split("/");
		var rfc = item2[0];
		console.log(rfc);
		$("#rfcGPHO").val(rfc);
		var item3 = arr[3].split("/");
		var rfcCliente = item3[1];
		console.log(rfcCliente);
		$("#rfcCliente").val(rfcCliente);
		var item4 = arr[4].split("/");
		var precio = parseFloat(item4[0]);
		console.log(precio);
		$("#precio").val(precio);
		$( "#sendQR" ).submit();
    }
}

var qr2 = "";
function cliente() {

    qr2 = $("#codigoCliente").val();
    console.log(qr2);
    if(qr2.slice(-1) == "_"){
    	var demo = qr2.replace("_", "");
        //console.log(demo);
    	var idCliente = demo.replace("?","_");
        console.log("idCliente: "+idCliente);
    	$("#idCliente").val(idCliente);
    	//console.log(idCliente);
		$( "#cliente" ).submit();
    }
}


$( document ).ready(function() {

    $("#manual").on("click",function(){
        console.log("manual");
        $("#automatico").css("display","block");
        $("#manual").css("display","none");
        $("#cliente").css("display","none");
        $("#clienteManual").css("display","block");
        $( "#email" ).focus();
    });

    $("#automatico").on("click",function(){
        console.log("Automatico");
        $("#manual").css("display","block");
        $("#automatico").css("display","none");
        $("#clienteManual").css("display","none");
        $("#cliente").css("display","block");
        $( "#codigoCliente" ).focus();
    });

    $("#clienteManual").validate({

        rules: {
            email: { required:true, email: true},    
            password: { required:true, minlength: 2}   
         },
        messages: {
            email: "Email incorrecto.",
            password: "Introduzca un password."
        },
        submitHandler: function(form){

            $.ajax({
                type: "POST",
                url: "/home",
                data: $(form).serialize(),
                beforeSend: function(){
                    
                },
                success: function(data){
                    console.log(data);
                    var object = JSON.parse(data);
                    if(object.type == "success"){
                        location.reload();
                    }
                    else if(object.type == "error"){
                        $.confirm({
                            title: 'Error!',
                            content: object.message,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'Intenta otra vez',
                                    btnClass: 'btn-red',
                                    action: function(){
                                    }
                                },
                                close: function () {
                                }
                            }
                        });
                    }
                }
            });
        }
    });

	/* Restricciones para newMember Clubestrella */
	$("#cliente").validate({

    	rules: {
            idCliente: { required:true, minlength: 3},    
	     },
        messages: {
            idCliente: "Introducir password.",
        },
        submitHandler: function(form){

            $.ajax({
                type: "POST",
                url: "/home",
                data: $(form).serialize(),
                beforeSend: function(){
					
				},
                success: function(data){
                	
          			var object = JSON.parse(data);
          			if(object.type == "success"){
          				location.reload();
          			}
                    else if(object.type == "error"){
                        $.confirm({
                            title: 'Error!',
                            content: object.message,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'Intenta otra vez',
                                    btnClass: 'btn-red',
                                    action: function(){
                                    }
                                },
                                close: function () {
                                }
                            }
                        });
                    }
                }
            });
        }
	});

    /* Formulario para dar de alta los puntos */
	$("#sendQR").validate({

    	rules: {
            test: { required:true, minlength: 3},    
	     },
        messages: {
            test: "Escanea el codigo QR.",
        },
        submitHandler: function(form){

            $.ajax({
                type: form.method,
                url: form.action,
                data: $(form).serialize(),
                beforeSend: function(){
					
				},
                success: function(data){
                    console.log(data);
                	var object = JSON.parse(data);
                    $("#sendQR")[0].reset();
          			if(object.type == "success"){
                        
                        $("#message").removeClass("bg-danger");
                        $("#message").addClass("bg-success");
                        $("#message").html("");
          				$("#message").append(object.message);
                        $("#pointsUser").html("");
                        $("#pointsUser").append(object.puntos);
          			}
                    else{

                        $("#message").removeClass("bg-success");
                        $("#message").addClass("bg-danger");
                        $("#message").html("");
                        $("#message").append(object.message);
                    }
          	
                }
            });
        }
	});
});