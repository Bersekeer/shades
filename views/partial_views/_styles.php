<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="/css/font-awesome/css/all.min.css" async>
<link rel="stylesheet" href="/css/jqueryUI/jquery-ui.min.css" async>

<link rel="stylesheet" href="/css/booty.min.css">
<link rel="stylesheet" href="/css/site.css">
<link rel="icon" type="image/png" href="/img/items/star.png" />
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" async >
<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet" async >
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" async >
<link rel="stylesheet" href="/css/lightpick.css">

<!-- menu mobile -->
<link rel="stylesheet" href="/css/offsidejs/offside.min.css" >
<link rel="stylesheet" href="/css/offsidejs/demo.min.css" >

<script type="text/javascript" src="js/jquery-3.3.1.min.js" ></script>
<script type="text/javascript" src="/js/jquery-ui.min.js" ></script>
<script type="text/javascript" src="/js/cookie/js.cookie.js"></script>
<script type="text/javascript" src="/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/node_modules/moment/min/locales.min.js"></script>
<script type="text/javascript" src="/js/lang.js"></script>
<script type="text/javascript" src="/js/lightpick_2020.js"></script>

<!-- hover animation navbar -->
<link rel="stylesheet" type="text/css" href="/css/hover/default.min.css" />
<link rel="stylesheet" type="text/css" href="/css/hover/component.min.css" />
<script src="/js/hover/modernizr.custom.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-49691355-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-49691355-1');
</script> -->