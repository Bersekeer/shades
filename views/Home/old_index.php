<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "views/partial_views/_styles.php"; ?> 
	<?php include "lang/languaje.php"; ?>	
	<meta name="authoring-tool" content="Adobe_Animate_CC">
	<meta name="keywords" content="<?php echo $_GLOBALS['keywords']; ?>">
	<title>Hotel Adhara Cancún</title>

	<!-- <?php
		if( !$detect->isMobile() ){
			$display = "display:block";
		
			if($_COOKIE['lang'] == 'es'){
	?>
		scripts de la animacion
		<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
		<script src="js/animacion.js"></script>
		<script src="js/canvas.js"></script>
	<?php
			}
			else{
	?>
		scripts de la animacion
		<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
		<script src="js/animacion_.js"></script>
		<script src="js/canvas_.js"></script>
	<?php
			}
		}
	?> -->
	<link rel="stylesheet" href="/css/reloj.css">
	

</head>
<body  onload="init();">

	

	<!-- Navbar mobile -->
    <?php include "views/partial_views/_navbar_mobile.php"; ?>

	<!-- Redes Sociales -->
	<?php include "views/partial_views/_redes.php"; ?>

	<div id="general">
		<!-- Navbar -->
		<?php include "views/partial_views/_navbar.php"; ?>

		<div class="quick-search">
			<form action="/room" method="POST" onsubmit="return dateCheck();" id="quickSearch">
				<ul class="list-check d-flex align-items-center brown_" id="custom-search">
					<li>
						<div class='input-group' id='startDate'>
							<div class="input-group-prepend calendar_">
								<span class="input-group-text">
									<i class="far fa-calendar-alt" style="color: white"></i>
								</span>
							</div>
							<input type='text' id="checkin" class="form-control widthCustom input_" name="checkin"  placeholder="<?php echo $_GLOBALS['search-from']; ?>" value="<?php echo (isset($_POST['checkin']) ? $_POST['checkin'] : $today); ?>" autocomplete="off"  />
				        </div>
				    </li>
					<li>
						<div class='input-group' id='endDate'>
							<div class="input-group-prepend calendar_">
								<span class="input-group-text">
									<i class="far fa-calendar-alt" style="color: white"></i>
								</span>
							</div>
							<input type='text' id="checkout" class="form-control widthCustom input_" name="checkout"  placeholder="<?php echo $_GLOBALS['search-to']; ?>" value="<?php echo (isset($_POST['checkout']) ? $_POST['checkout'] : $tomorrow); ?>" autocomplete="off"  />
				        </div>
					</li>
					<li class="no-desktop" style="padding-left: 12px;">
						<input type="hidden" value="<?php echo $_SESSION['csrf_token']; ?>" readonly name="csrf_token">
						<input type="hidden" value="true" readonly name="quick">
						<button type="submit" id="btn_quick"><img src="img/btn_search.png" alt="Buscar" id="img_search"></button>
					</li>
				</ul>
				<!-- <div class="clearfix"></div> -->
				<ul class="list-check d-flex align-items-center justify-content-center no-flex brown_">
					<li>
						<p id="quick-date"><?php echo $_GLOBALS['quick-date']; ?></p>
					</li>
					<li class="monze-bold" id="li-price">
						<!-- font size 16 -->
						<p style="font-size: 12px;" id="quick-price">$ <?php echo round($room->price)." ".$_GLOBALS['reserva-currency']; ?></p>
					</li>
					<li class=" monze-light">
						<p id="quick-tarifa"><span id="span-tarifa"><?php echo $_GLOBALS['quick-tarifa']; ?></span></p>
					</li>
				</ul>
				<ul class="list-check d-flex align-items-center no-flex brown_" style="border-radius: 7px;">
					<li style="margin:0px;padding-left: 3px;">
						<p id="quick-whats"><?php echo $_GLOBALS['quick-whats']; ?></p>
					</li>
					<li><a href=""><img src="img/whats_icon.png" alt="Whatsapp" id="quick-img" style="<?php echo $_GLOBALS['quick-style'];?>"></a></li>
					<li>
						<input type="hidden" value="<?php echo $_SESSION['csrf_token']; ?>" readonly name="csrf_token">
						<input type="hidden" value="true" readonly name="quick">
						<button type="submit" id="btn_quick"><img src="img/btn_search.png" alt="Buscar" id="img_search"></button>
					</li>
				</ul>
				<div class="row" style="margin: 0px;">
					<div class="col-xs-12 col-sm-6 col-md-6 bg-danger text-white" id="warning"></div>
				</div>
			</form>
		</div>

		<!-- prueba timer -->
		<div class="reloj">
			<div class="row">
				<div class="col-4 col-sm-6 col-md-4">
					
					<img src="img/buenfin.png" alt="Buen Fin" id="buen-fin">
				</div>
				<div class="col-8 col-sm-6 col-md-8">
					<p id="quedan" class="time-rest">FALTAN:</p>
					<div class="inline-block">
						<h2 class="counter1" id="dias">00</h2>
						<span class="pre block">días</span>
					</div>
					<div class="inline-block">
						<h2 class="counter" id="horas">00</h2>
						<span class="pre block">horas</span>
					</div>
					<div class="inline-block">
						<h2 class="counter" id="min">00</h2>
						<span class="pre block">min</span>
					</div>
					<div class="inline-block">
						<h2 class="counter" id="seg">00</h2>
						<span class="pre block">seg</span>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(getTime());
			function getTime() {
				now = new Date();
				fecha = new Date("2019-11-15 00:00:00 UTC");
				days = (fecha - now) / 1000 / 60 / 60 / 24;
				daysRound = Math.floor(days);
				hours = (fecha - now) / 1000 / 60 / 60 - (24 * daysRound);
				hoursRound = Math.floor(hours);
				minutes = (fecha - now) / 1000 /60 - (24 * 60 * daysRound) - (60 * hoursRound);
				minutesRound = Math.floor(minutes);
				seconds = (fecha - now) / 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
				secondsRound = Math.round(seconds);
				if (daysRound <= "-1") {
					//   IMPORTANTE  //
					//Si el conteo regresivo del script el valor de los días es mayor a -1 se para el script, 
					//ya que la fecha esperada se a cumplido, es necesaria este línea de código ya que si no se pone 
					//seguiria el conteo regresívo pero en valores negativos.
				}
				else{
					document.getElementById('dias').innerHTML = daysRound;
					document.getElementById('horas').innerHTML = hoursRound;
					document.getElementById('min').innerHTML = minutesRound;
					document.getElementById('seg').innerHTML = secondsRound;
				}
				newtime = window.setTimeout("getTime();", 1000);
			}

		</script>

		<!-- END prueba timer -->
		<!-- <div style="width: 100%;height: auto;background-image: url('/img/background.png');<?php echo $display; ?>">
			
			<div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:1500px; height:500px">
				<canvas id="canvas" width="1500" height="1300" style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
				<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:1500px; height:1300px;  left: 0px; top: 0px; display: block;">
				</div>
			</div>
			<div id='_preload_div_' style='display: inline-block;'>	
				<?php 
					if($_COOKIE['lang'] == "es"){
					
				?>
					<span style='display: inline-block; height: 100%; vertical-align: middle;'></span>	
					<img src=images/_preloader.gif style='vertical-align: middle; max-height: 100%'/>
					
				<?php
					}
					else{
				?>
					<img src=imagesingles/_preloader.gif style='vertical-align: middle; width: 100%'/>
				<?php
					}
				?>
			</div>
		
		</div>  -->

		<div class="contain" style="width: 100%;background-image: url('/img/background.png');">

			<div  id="wrapper-content" style="max-width: 1500px">
				<!-- No desktop -->
				<img src="<?php echo $_GLOBALS['home-image']; ?>" alt="" class="img-fluid" id="home_test">
				<div class="no-desktop">
					<div class="tarifa_magica brown_">
						<ul style="list-style: none;padding:0px;padding-left:15px;" class="d-flex align-items-center list-check">
							<li style="width: 100px;text-align: center;">
								<p style="font-size: 7px;letter-spacing: 1px;"><?php echo $_GLOBALS['quick-date-mob']; ?></p>
							</li>
							<li>
								<p style="font-size: 14px;font-weight: 800;">$ <?php echo round($room->price)." ".$_GLOBALS['reserva-currency']; ?></p>
							</li>
							<li style="margin-right: 6px;">
								<p style="font-size: 8px;text-align: center;"><span style="font-size: 7px;"><?php echo $_GLOBALS['quick-tarifa']; ?></p>
							</li>
							<li>
								<a href=""><img src="img/whats_icon.png" alt="Whatsapp" style="width: 40px;margin-top: -15px;"></a>
							</li>
						</ul>
						
					</div>
				</div>
				<div id="home-mob">
					<img src="<?php echo $_GLOBALS['url-home-800']; ?>" alt="Hotel Adhara Cancún" class="img-fluid" id="home_moe">
					<img src="<?php echo $_GLOBALS['url-home-500']; ?>" alt="Hotel Adhara Cancún" class="img-fluid" id="home_">
				</div>
				<!-- test mobile -->
				<div class="wrapper_text">
					<div class="text_box">

						<h4 class="tittle" id="first_"><?php echo $_GLOBALS['home-tittle-2']; ?></h4>
						<p><?php echo $_GLOBALS['home-descrip']; ?></p>
						<p><?php echo $_GLOBALS['home-descrip2']; ?></p>

					</div>
				</div>
				<div class="wrapper_img">
					<img src="img/places/room.png" alt="Cuartos" class="img-fluid desktop">
					<img src="img/places/mobile/cuarto.png" alt="Cuartos" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 offset-sm-6 col-md-6 offset-md-6 box_" id="room_text">
							<div class="animation fadeIn-right" style="background-color: rgba(131, 92, 87, 0.7);">
								<div class="text_cage">
									<h2><?php echo $_GLOBALS['home-rooms']; ?></h2>
									<p><?php echo $_GLOBALS['rooms-p']; ?></p>

									<!-- En ingles no va esta parte de texto -->
									<p id="bye_r" style="margin-top: 30px;text-align: center;"><?php echo $_GLOBALS['rooms-p2']; ?></p>
									<a href="/showroom" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="wrapper_text">
					<div class="text_box" id="flower_text">
						<p><?php echo $_GLOBALS['somos-p']; ?></p>
						<img src="img/items/flower.png" alt="Flower" class="img-fluid" style="width: 60px;">
					</div>
				</div>

				<div class="wrapper_img">
					<img src="img/places/pool.png" alt="Alberca" class="img-fluid desktop">
					<img src="img/places/mobile/pool.png" alt="Alberca" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 col-md-6 box_" id="pool_text">
							<div class="animation fadeIn-left" style="background-color: rgba(137, 44, 121, 0.7);">
								<div class="text_cage">
									<h2><?php echo $_GLOBALS['alberca-h']; ?></h2>
									<p><?php echo $_GLOBALS['alberca-p3']; ?></p>
									<a href="/pool" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper_text">
					<div class="text_box" id="bird_text">
						<p><?php echo $_GLOBALS['somos-p2']; ?></p>
						<img src="img/items/bird.png" alt="Bird" style="width: 60px;">
					</div>
				</div>

				<div class="wrapper_img">
					<img src="img/places/breakfast.png" alt="Adhara Grill" class="img-fluid desktop">
					<img src="img/places/mobile/grill.png" alt="Adhara Grill" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 offset-sm-6 col-md-6 offset-md-6 box_" id="grill_text">
							<div class="animation fadeIn-right" style="background-color: rgba(91, 26, 21, 0.7);">	
								<div class="text_cage">
									<h2><?php echo $_GLOBALS['restaurante-h']; ?></h2>
									<img src="img/logos/grill_logo.png" alt="Adhara Grill" id="grill-logo">
									<p><?php echo $_GLOBALS['restaurante-p']; ?></p>
									<a href="/adharagrill" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="wrapper_text">
					<div class="text_box" id="adhara_text">
						<p><?php echo $_GLOBALS['somos-p4']; ?></p>
						<img src="img/items/star.png" alt="Adhara Cancun" style="width: 60px;">
					</div>
				</div>

				<div class="wrapper_img">
					<img src="img/places/beach.png" alt="Oktrip" class="img-fluid desktop">
					<img src="img/places/mobile/oktrip.png" alt="Oktrip" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 col-md-6 box_" id="oktrip_text">
							<div class="animation fadeIn-left" style="background-color: rgba(226, 123, 54, 0.7);">
								<div class="text_cage">
									<h2><?php echo $_GLOBALS['grupos-h']; ?></h2>
									<div class="row" style="margin: 0px;padding: 0px;">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<img src="img/logos/oktrip_logo.png" id="oktrip_logo" alt="Oktrip">
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 no-mobile">
											<img src="img/logos/club_logo.png" id="club_logo" alt="Club Estrella">
										</div>
									</div>
									<p><?php echo $_GLOBALS['grupos_label']; ?></p>
									<a href="https://oktrip.mx/" target="_blank" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="wrapper_text">
					<div class="text_box" id="adhara_text">
						<p><?php echo $_GLOBALS['somos-p5']; ?></p>
						<img src="img/items/suitcase.png" alt="Oktrip" style="width: 40px;">
					</div>
				</div>

				<div class="wrapper_img">
					<img src="img/places/dinner.png" alt="Adhara Grill" class="img-fluid desktop">
					<img src="img/places/mobile/postre.png" alt="Adhara Grill" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 offset-sm-6 col-md-6 offset-md-6 box_" id="eventos_text">
							<div class="animation fadeIn-right" style="background-color: rgba(52, 22, 47, 0.7);">
								<div class="text_cage">
									<img src="img/logos/eventos_logo.png" alt="Eventos Adhara" id="eventos_logo">
									<p><?php echo $_GLOBALS['eventos-p']; ?></p>
									<a href="/eventos" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="wrapper_text">
					<div class="text_box" id="event_text">
						<p style="margin-bottom: 15px;"><?php echo $_GLOBALS['home-magia-h2']; ?></p>
						<p><?php echo $_GLOBALS['home-magia-p2']; ?></p>
						<img src="img/items/light.png" alt="Eventos Adhara" style="width: 40px;">
					</div>
				</div>

				<div class="wrapper_img">
					<img src="<?php echo $_GLOBALS['club-img']; ?>" alt="Adhara Grill" class="img-fluid desktop">
					<img src="img/places/mobile/clubestrella.png" alt="Adhara Grill" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 col-md-6 box_" id="club_text">
							<div class="animation fadeIn-left" style="background-color: rgba(18, 17, 15, 0.7);">
								<div class="text_cage" id="club_">
									<h2><?php echo $_GLOBALS['clubestrella-h']; ?></h2>
									<img src="img/logos/club_logo.png" alt="Club Estrella Grill" id="club-logo">
									<p class="text-cent"><?php echo $_GLOBALS['clubestrella-p']; ?></p>

									<a href="https://clubestrella.mx/" target="_blank" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
						<!-- <div class="col-xs-12 col-sm-6 col-md-6 no-mobile">
							<div class="text_cage" id="estrella_">
								
								<p class="text-cent3">¡También cambia tus puntos por diferentes premios!</p>
								<p class="text-cent2">Cerveza.</p>
								<p class="text-cent2">Ascenso a habitación de categoria superior.</p>
								<p class="text-cent2">Desayuno Buffet o Menú Ejecutivo.</p>
								<p class="text-cent2">Cocodrillo Grilled Sándwich.</p>
								<p class="text-cent2">Noche Gratis Hotel Adhara Hacienda Cancún.</p>
								<p class="text-cent2">Cafetería Dolce Gusto.</p>
								<p class="text-cent2">Ipod Touch 64 GB.</p>
								<img src="img/items/coffee.png" alt="Club Estrella" id="coffe_logo">
								<p class="text-cent2">Imágenes ilustrativas, los modelos pueden variar.</p>
							</div>
						</div> -->
					</div>
				</div>
				<div class="wrapper_text">
					<div class="text_box" id="airplane_text">
						<p><?php echo $_GLOBALS['shuttle-p']; ?></p>
						<img src="img/items/airplane.png" alt="Transportacion" style="width: 40px;">
					</div>
				</div>
				<div class="wrapper_img">
					<img src="img/places/airplane.png" alt="Transportación" class="img-fluid desktop">
					<img src="img/places/mobile/shuttle.png" alt="Transportación" class="img-fluid mobile">
					<div class="row text_side">
						<div class="col-xs-12 col-sm-6 offset-sm-6 col-md-6 offset-md-6 box_" id="plane_text">
							<div class="animation fadeIn-right" style="background-color: rgba(0, 33, 61, 0.7);">
								<div class="text_cage" >
									<h2><?php echo $_GLOBALS['home-magia-h3']; ?></h2>
									<p class="text-cent"><?php echo $_GLOBALS['home-magia-p3']; ?></p>
									<p class="text-cent"><?php echo $_GLOBALS['home-magia-p4']; ?></p>
									<a href="/shuttle" ><?php echo $_GLOBALS['home-details']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="wrapper_text" style="height: 300px;">
					<div class="text_box">
						<a href="https://www.tripadvisor.com.mx/Hotel_Review-g150807-d154412-Reviews-Adhara_Hacienda_Cancun-Cancun_Yucatan_Peninsula.html" target="_blank">
							<img src="img/logos/tripadvisor.png" id="trip_logo" alt="Tripadvisor">
						</a>
					</div>

				</div>
				
				<div id="wrapper_footer">
					<?php include "views/partial_views/_footer.php"; ?>
				</div>
			</div>
		</div>
	</div>
	 <!-- Site Overlay 
    <div class="site-overlay"></div>-->
	<!-- Preloading -->
	<!-- <?php include "views/partial_views/_preloading.php"; ?> -->

</body>

<?php include "views/partial_views/_scripts.php"; ?>
 

<script type="text/javascript">

	function dateCheck() {

        var checkin = new Date(quickSearch.checkin.value);
        var checkout = new Date(quickSearch.checkout.value);
        var today = new Date();
        console.log(today);
        console.log(checkin);
        console.log(checkout);
        if(checkin >= checkout || checkout <= checkin){
         	$("#warning").css("display","block");
         	$("#warning").html("");
         	$("#warning").append("<p style='margin-bottom:0px;'>Fechas Incorrectas</p>");
         	//console.log("Error fechas malas");
         	return false;
        }
        else if(checkin <= today || checkout <= today){
        	$("#warning").css("display","block");
         	$("#warning").html("");
         	$("#warning").append("<p style='margin-bottom:0px;'>La fecha debe ser mayor a hoy</p>");
         	return false;
        }
        else{
         	//console.log("All good");
         	return true
        }
	}
	
	$(document).ready(function(){

		$(window).scroll(function(){

	        if($("#room_text").visible(true)){
	        	$("#room_text").addClass("letGo");
	        }
	        else{
	        	$("#room_text").removeClass("letGo");
	        }

	        if($("#pool_text").visible(true)){
	        	$("#pool_text").addClass("letGo");
	        }
	        else{
	        	$("#pool_text").removeClass("letGo");
	        }
	        if($("#grill_text").visible(true)){
	        	$("#grill_text").addClass("letGo");
	        }
	        else{
	        	$("#grill_text").removeClass("letGo");
	        }

	        if($("#oktrip_text").visible(true)){
	        	$("#oktrip_text").addClass("letGo");
	        }
	        else{
	        	$("#oktrip_text").removeClass("letGo");
	        }

	        if($("#eventos_text").visible(true)){
	        	$("#eventos_text").addClass("letGo");
	        }
	        else{
	        	$("#eventos_text").removeClass("letGo");
	        }

	        if($("#club_text").visible(true)){
	        	$("#club_text").addClass("letGo");
	        }
	        else{
	        	$("#club_text").removeClass("letGo");
	        }

	        if($("#plane_text").visible(true)){
	        	$("#plane_text").addClass("letGo");
	        }
	        else{
	        	$("#plane_text").removeClass("letGo");
	        }
    	});


	});

</script>

</html>